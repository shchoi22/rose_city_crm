process.env.NODE_ENV = process.env.NODE_ENV || 'development';

var	express = require('express'),
    config = require('./config/config'),
    bodyParser = require('body-parser'),
    methodOverride = require('method-override'),
    mongoose = require('mongoose'),
    morgan = require('morgan'),
    session = require('express-session'),
    passport = require('passport'),
    mongoStore = require('connect-mongo')(session),
    cookieParser = require('cookie-parser'),
    http = require('http'),
    flash = require('connect-flash');

var app = express();

// models
var member = require('./app/models/member_server_model');
var user = require('./app/models/user_server_model');
var team = require('./app/models/team_server_model');
var note = require('./app/models/note_server_model');
var insurance = require('./app/models/insurance_server_model');
var log = require('./app/models/log_server_model');

var pass = require('./config/passport');

// config
app.use(express.static(__dirname + '/public'));
app.use(bodyParser.json());
app.use(bodyParser.json({ type: 'application/vnd.api+json'}));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(methodOverride('X-HTTP-Method-Override'));
app.use(morgan('dev'));
app.use(cookieParser());

app.use(session({
  secret: 'MEAN',
  store: new mongoStore({
  	resave: true,
  	saveUninitialized: true,
  	url: config.db,
  	collection: 'sessions'
  })
}));
app.use(flash());

app.use(passport.initialize());
app.use(passport.session());

require('./app/routes/notes_server_routes')(app);
require('./app/routes/members_server_routes')(app);
require('./app/routes/teams_server_routes')(app);
require('./app/routes/contacts_server_routes')(app);
require('./app/routes/insurances_server_routes')(app);
require('./app/routes/enums_server_routes')(app);
require('./app/routes/user_server_routes')(app);
require('./app/routes/logs_server_routes')(app);
require('./app/routes/index_server_routes')(app);

mongoose.connect(config.db);
console.log(config.db);
app.set('port', (process.env.PORT || 5000));

app.listen(app.get('port'), function() {
  console.log("Node app is running at localhost:" + app.get('port'));
});

module.exports = app;

if (process.env.NODE_ENV === 'production') {
  setInterval(function() {
    http.get('http://rosecityrollers.herokuapp.com/');
  }, 300000); //ping heroku every 5 mins.
}