//TODO: 
//Need to implement auth control for all actions. 

var mongoose = require('mongoose'),
	Member = mongoose.model('Member'),
	Team = mongoose.model('Team'),
	Note = mongoose.model('Note'),
	Log = mongoose.model('Log'),
	_ = require('underscore'),
	csv = require('express-csv'),
	transform = require('../helpers/transform'),
	config = require('../../config/config'),
	diff = require('../helpers/simple_diff');

//TODO: Currently just logging to error, need to refactor so that
//on create, validation errors are caught and handled
Member.on('error', function(err) {
  console.log(err);
});

Team.on('error', function(err) {
  console.log(err);
});

Note.on('error', function(err) {
  console.log(err);
});

exports.create = function(req, res) {
	var member = new Member(req.body);
 
	member.save(function(err) {
		if(err) {
			return res.status(400).json({
				message: getErrorMessage(err)
			});
		} else if (member.affiliation) {
			// Adds reference of this member to its team members array
			Team.findById(member.affiliation).update({'$addToSet': { 'members': member._id}},function(err, member){
        		if(err){
          			return res.status(400).json({
				    	message: getErrorMessage(err)
			    	});
        		}
      		});
		}
		var log = new Log({
        	action: 'Create',
        	changed_id: member._id,
        	changed_type: 'Member',
        	diff: diff({}, member._doc),
        	created_by: req.user._id
        });
        log.save();
		
		res.json(member);
	});
};

exports.list = function(req, res) {
  //TODO: NEED TO ACCOUNT FOR FULL NAME SEARCH, LOOK FOR SPACES IN SEARCH TEXT
  var search_params = [{}], //regex query params for `or` statement
      limit = {}; //limiting the search results

  if ('search_text' in req.query) {
  	var re = new RegExp(req.query.search_text, 'i');
  	search_params = [{ 'first_name': { $regex: re }}, { 'last_name': { $regex: re }}, { 'skate_name': { $regex: re }}];
  	limit = 10;
  }

  Member.find({}).or(search_params).sort('-created_at').limit(limit).exec(function(err, members) {
  	if (err) {
  		return res.status(400).json({
  			message: getErrorMessage(err)
  		});
    } else {
    	res.json(members);
    }
  });
};

//For downloading csv of member data
exports.report = function(req, res) {
	Member.find({}).populate('affiliation', 'name').exec(function(err, members) {
    console.log("request hit")
		if (err) {
			return res.status(400).json({
				message: getErrorMessage(err)
			});
		} else {

      var data = [config.report_columns];

      members.forEach(function(element, index, array) {
        data.push(transform(element));
      });

      res.csv(data);
		}
	});
}

exports.memberById = function(req, res, next, id) {
	Member.findOne({'_id': id})
    .populate('affiliation', 'name _id')
	  .populate('notes', 'created_at content') 
	  .exec(function(err, member) {
		  if (err) {
			  return next(err);
		  } 
		  if(!member) {
			  return next(new Error('No member found with id ' + id));
		  }
		  req.member = member;
		  next();
  	});
};

exports.read = function(req, res) {
	res.json(req.member);
};

exports.update = function(req, res) {
	var member = req.member;
	var old_member;
	
	var oldTeam;

	member = _.extend(member, req.body);

	Member.findById( member._id, function(err, _member) {
		old_member = _member;

		if (!_.isEqual(diff(old_member._doc, member._doc),{})) {

			var difference =  diff(old_member._doc, member._doc);
			difference.hasOwnProperty('contacts') ? delete difference.contacts : null;
			difference.hasOwnProperty('notes') ? delete difference.notes : null;
			difference.hasOwnProperty('insurances') ? delete difference.insurances : null;

			if (!_.isEqual(difference,{})) {
				var log = new Log({
					action: 'Update',
        			changed_id: member._id,
        			changed_type: 'Member',
        			diff: difference,
        			created_by: req.user._id
				});

				log.save();
			}
		}
	});
	
	// Removes member from old affiliation
  	Member.findById( member._id, function(err, _member){
    	oldTeam = _member.affiliation;
    	if(oldTeam){
        Team.findById(oldTeam).update({'$pull': {'members':member._id}}, function(err, _member) {
          if(err){
            return res.status(400).json({
              message: getErrorMessage(err)
            });        
          }
        });
      }
    });
  
	member.save(function(err) {
		if(err) {
      var msg = getErrorMessage(err);
      return res.status(400).json({ message: msg});
      
		} else{ 
      		if (member.affiliation) {
				// Adds reference of this member to its team members are
        		Team.findById(member.affiliation).update({'$addToSet': { 'members': member._id}},function(err, member){
        			if(err){
            			return res.status(400).json({ 
            				message: getErrorMessage(err) 
            			});
          			}
      			});
			}
			res.json(member);
		};
	});
};

exports.delete = function(req, res) {
	var member = req.member;

	var log = new Log({
		action: 'Delete',
		changed_id: member._id,
        changed_type: 'Member',
        diff: diff(member._doc, {}),
        created_by: req.user._id
	});

	member.remove(function(err) {
		if(err) {
			return res.status(400).json({
				message: getErrorMessage(err)
			});
		} else if (member.affiliation) {
			Team.findById(member.affiliation).update({'$pull': {'members':member._id}}, function(err, member) {
				if(err){
					return res.status(400).json({
						message: getErrorMessage(err)
					});
				};
			});
		}
		log.save();
		res.json(member);
	});
};

//Error handling helper method
var getErrorMessage = function(err) {
  var errors = [];
	if (err.errors) {
		for (var errName in err.errors) {
			if (err.errors[errName].message) {
        errors.push(err.errors[errName].message);
			} 
		}
	} else {
		return 'Unknown server error';
	}
  return errors;
};