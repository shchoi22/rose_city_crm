var mongoose = require('mongoose'),
	Note = mongoose.model('Note'),
	Member = mongoose.model('Member'),
	_ = require('underscore');

exports.create_att_member = function(req, res) {
	var member = req.member;
	var note = new Note(req.body);

	note.save(function(err, note) {
		member.notes.push(note._id);
		member.save(function(err, member) {
			if(err) {
				return res.status(400).json({
					message: getErrorMessage(err)
				});
			} else {
				res.json(member);
			}
		});
	});
};

exports.list_for_member = function(req, res) {
	Note.find({attached_to: req.member._id}).sort('-created_at').exec(function(err, notes) {
		if (err) {
			return res.status(400).json({
				message: getErrorMessage(err)
			});
		} else {
			res.json(notes)
		}
	});
};
//TODO: Create and attach notes to teams.
/*
exports.create_att_team = function(req, res) {
};
*/

exports.noteById = function(req, res, next, id) {
	Note.findOne({'_id': id},function(err, note) {
		if (err) {
			return next(err);
		} 
		if(!note) {
			return next(new Error('No note found with id ' + id));
		}
		req.note = note;
		next();
	});
};

exports.read = function(req, res) {
	res.json(req.note);
};

exports.update = function(req, res) {
	var note = req.note;
	note = _.extend(note, req.body);
	note.save(function(err, note) {
		if(err) {
			return res.status(400).json({
				message: getErrorMessage(err)
			});
		} else {
			res.json(note);
		}
	});
};

exports.delete_att_member = function(req, res) {
	var note = req.note;
	var member = req.member;
	member.notes.pull({_id: note._id});
	note.remove(function(err) {
		member.save(function(err, member) {
			if(err) {
				return res.status(400).json({
					message: getErrorMessage(err)
				});
			} else {
				res.json(member);
			}
		});
	});
};

//Error handling helper method
var getErrorMessage = function(err) {
  var errors = [];
	if (err.errors) {
		for (var errName in err.errors) {
			if (err.errors[errName].message) {
        errors.push(err.errors[errName].message);
			} 
		}
	} else {
		return 'Unknown server error';
	}
  return errors;
};