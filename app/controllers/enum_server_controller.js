var mongoose = require('mongoose'),
	Member = mongoose.model('Member'),
	Note = mongoose.model('Note'),
	Contact = mongoose.model('Contact'),
	User = mongoose.model('User'),
	_ = require('underscore');

//TODO: Refactor to loop over enum fields. Remove hard coding of specific fields.
exports.enumValues = function(req, res) {
	res.json({
		members: {
			member_type: Member.schema.path('member_type').enumValues,
			gender: Member.schema.path('gender').enumValues,
			skate_size: Member.schema.path('skate_size').enumValues,
			shirt_size: Member.schema.path('shirt_size').enumValues,
			knee_pad_size: Member.schema.path('knee_pad_size').enumValues,
			elbow_pad_size: Member.schema.path('elbow_pad_size').enumValues,
			wrist_size: Member.schema.path('wrist_size').enumValues,
			helmet_size: Member.schema.path('helmet_size').enumValues,
			state: Member.schema.path('state').enumValues
		},
		contacts: {
			contact_type: Contact.schema.path('contact_type').enumValues
		},
		users: {
			roles: User.schema.path('role').enumValues
		}		
	});
};

	