var mongoose = require('mongoose'),
  User = mongoose.model('User'),
  passport = require('passport'),
  _ = require('underscore'),
  ObjectId = mongoose.Types.ObjectId;

//TODO: Currently just logging to error, need to refactor so that
//on create, validation errors are caught and handled
User.on('error', function(err) {
  console.log(err);
});

/**
 * Create user
 * requires: {username, password, email}
 * returns: {email, password}
 */
exports.create = function (req, res, next) {
  var newUser = new User(req.body);
  newUser.provider = 'local';

  newUser.save(function(err) {
    if (err) {
      return res.json(400, err);
    }

    req.logIn(newUser, function(err) {
      if (err) return next(err);
      return res.json(newUser.user_info);
    });
  });
};

exports.ensureAdmin = function (req, res, next) {
  var user = req.user;
  if (user.role === 'admin') {
    return next();
  }
  res.send(403, 'NOT_AUTHORIZED');
};

exports.list = function (req, res, next) {
  User.find({}).exec(function(err, users) {
    if (err) {
      return res.status(400).send({
        message: err
      });
    } else {
      res.json(users);
    }
  });
};

/**
 *  Show profile
 *  returns {username, profile}
 */
exports.show = function (req, res, next) {
  var userId = req.params.userId;

  User.findById(ObjectId(userId), function (err, user) {
    if (err) {
      return next(new Error('Failed to load User'));
    }
    if (user) {
      res.json(200, user.user_info);
    } else {
      res.send(404, 'USER_NOT_FOUND')
    }
  });
};

exports.update = function(req, res) {
  var userId = req.params.userId;

  User.findById(ObjectId(userId), function (err, user) {
    if (err) {
      return next(new Error('Failed to load User'));
    }
    if (user) {
      user = _.extend(user, req.body);
      user.save(function(err, data) {
        if(err) {
          return res.status(400).send({
            message: err
          });
        } else {
          res.json(data.user_info);
        }
      });
    } else {
      res.send(404, 'USER_NOT_FOUND')
    }
  });
};


/**
 *  Username exists
 *  returns {exists}
 */
 //TODO: Need to fix returning value not being correct
exports.exists = function (req, res, next) {
  var username = req.params.username;
  User.findOne({ username : username }, function (err, user) {
    if (err) {
      return next(new Error('Failed to load User ' + username));
    }
    if(user) {
      res.status(200).json({exists: true});
    } else {
      res.status(200).json({exists: false});
    }
  });
}