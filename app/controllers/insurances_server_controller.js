var mongoose = require('mongoose'),
	Member = mongoose.model('Member'),
	Insurance = mongoose.model('Insurance'),
	_ = require('underscore');

//TODO: Currently just logging to error, need to refactor so that
//on create, validation errors are caught and handled
Insurance.on('error', function(err) {
  console.log(err);
});


exports.create = function(req, res) {
	var member = req.member;
	var insurance = new Insurance(req.body);

	member.insurances.push(insurance);

	member.save(function(err, member) {
		if(err) {
			return res.status(400).json({
				message: getErrorMessage(err)
			});
		} else {
			res.json(member);
		}
	});
};

exports.list = function(req, res) {
	var member = req.member;
	if (!member.insurances) {
		return res.status(400).json({
				message: getErrorMessage(err)
			});
	} else {
		res.json(member.insurances);
	}
};

exports.insuranceById = function(req, res, next, id) {
	var member = req.member;

	var insurance = member.insurances.id(id);
	
	if(!insurance) {
		return next(new Error('No insurance found with id ' + id));
	}
	
	req.insurance = insurance;
	next();
};

exports.read = function(req, res) {
	res.json(req.insurance);
};

exports.update = function(req, res) {
	var member = req.member;
	var insurance = req.insurance;

	insurance = _.extend(insurance, req.body);
	
	member.save(function(err, member) {
		if(err) {
			return res.status(400).json({
				message: getErrorMessage(err)
			});
		} else {
			res.json(member);
		}
	});
};

exports.delete = function(req, res) {
	var insurance = req.insurance;
	var member = req.member;

	member.insurances.id(insurance._id).remove();

	member.save(function(err, member) {
		if(err) {
			return res.status(400).json({
				message: getErrorMessage(err)
			});
		} else {
			res.json(member);
		}
	});
};

//Error handling helper method
var getErrorMessage = function(err) {
  var errors = [];
	if (err.errors) {
		for (var errName in err.errors) {
			if (err.errors[errName].message) {
        errors.push(err.errors[errName].message);
			} 
		}
	} else {
		return 'Unknown server error';
	}
  return errors;
};