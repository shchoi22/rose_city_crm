var mongoose = require('mongoose'),
	Member = mongoose.model('Member'),
	Contact = mongoose.model('Contact'),
	_ = require('underscore');


//TODO: Currently just logging to error, need to refactor so that
//on create, validation errors are caught and handled
Contact.on('error', function(err) {
  console.log(err);
});


exports.create = function(req, res) {
	var member = req.member;
	var contact = new Contact(req.body);

	member.contacts.push(contact);

	member.save(function(err, member) {
		if(err) {
			return res.status(400).json({
				message: getErrorMessage(err)
			});
		} else {
			res.json(member);
		}
	});
};

exports.list = function(req, res) {
	var member = req.member;
	if (!member.contacts) {
		return res.status(400).json({
				message: getErrorMessage(err)
			});
	} else {
		res.json(member.contacts);
	}
};

exports.contactById = function(req, res, next, id) {
	var member = req.member;

	var contact = member.contacts.id(id);
	
	if(!contact) {
		return next(new Error('No contact found with id ' + id));
	}
	
	req.contact = contact;
	next();
};

exports.read = function(req, res) {
	res.json(req.contact);
};

exports.update = function(req, res) {
	var member = req.member;
	var contact = req.contact;

	contact = _.extend(contact, req.body);
	
	member.save(function(err, member) {
		if(err) {
			return res.status(400).json({
				message: getErrorMessage(err)
			});
		} else {
			res.json(member);
		}
	});
};

exports.delete = function(req, res) {
	var contact = req.contact;
	var member = req.member;

	member.contacts.id(contact._id).remove();

	member.save(function(err, member) {
		if(err) {
			return res.status(400).json({
				message: getErrorMessage(err)
			});
		} else {
			res.json(member);
		}
	});
};

//Error handling helper method
var getErrorMessage = function(err) {
  var errors = [];
	if (err.errors) {
		for (var errName in err.errors) {
			if (err.errors[errName].message) {
        errors.push(err.errors[errName].message);
			} 
		}
	} else {
		return 'Unknown server error';
	}
  return errors;
};