//Sample serverside controller
var path = require('path');
exports.render = function(req, res) {
	res.sendFile( path.join(__dirname, '../../public/views/index.html'));
}