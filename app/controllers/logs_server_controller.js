var mongoose = require('mongoose'),
	Member = mongoose.model('Member'),
	Log = mongoose.model('Log'),
	_ = require('underscore');

exports.list = function(req, res) {
	Log.find({changed_id: req.member._id}).populate('created_by', 'email username').sort('-created_at').exec(function(err, logs) {
		if (err) {
			return res.status(400).json({
				message: getErrorMessage(err)
			});
		} else {
			res.json(logs)
		}
	});
};

//Error handling helper method
var getErrorMessage = function(err) {
  var errors = [];
	if (err.errors) {
		for (var errName in err.errors) {
			if (err.errors[errName].message) {
        errors.push(err.errors[errName].message);
			} 
		}
	} else {
		return 'Unknown server error';
	}
  return errors;
};