var mongoose = require('mongoose'),
    Team = mongoose.model('Team'),
    Member = mongoose.model('Member'),
    _ = require('underscore');

//TODO: Currently just logging to error, need to refactor so that
//on create, validation errors are caught and handled
Team.on('error', function(err) {
  console.log(err);
});

exports.create = function(req, res){
  var team = new Team(req.body);
  
  team.save(function(err){
    if(err) {
			return res.status(400).json({
				message: getErrorMessage(err)
			});
		} else {
			res.json(team);
		}
  });
};

exports.list = function(req, res){
  Team.find({}).select('name').exec(function(err, teams) {
		if (err) {
			return res.status(400).json({
				message: getErrorMessage(err)
			});
		} else {
			res.json(teams)
		}
	});
};

exports.teamById = function(req, res, next, id){
  Team.findOne({'_id': id})
  .populate({
    path: 'members'
  })
  .exec(function(err, team){
    if (err) { return next(err);	} 
		if(!team) {
			return next(new Error('No team found with id ' + id));
	}
	req.team = team;
	next();
  });
};

exports.read = function(req, res){
  res.json(req.team);
};

exports.update = function(req, res) {
	var team = req.team;
	team = _.extend(team, req.body)
	
	team.save(function(err, data) {
		if(err) {
			return res.status(400).json({
				message: getErrorMessage(err)
			});
		} else {
			if (data.members) {
				for(var i in data.members) {
					Member.findOneAndUpdate({"_id" : data.members[i]._id}, { 
						"$set": {
							"affiliation": data._id
						}
					} , function (err,docs) {
						if(err) {
							return res.status(400).json({
								message: getErrorMessage(err)
							});
						} 
					});
				}
			}
			res.json(team);
		}
	});
};

exports.delete = function(req, res) {
	var team = req.team;

	team.remove(function(err) {
		if(err) {
			return res.status(400).json({
				message: getErrorMessage(err)
			});
		} else {
			res.json(team);
		}
	});
};

//Error handling helper method
var getErrorMessage = function(err) {
	if (err.errors) {
		for (var errName in err.errors) {
			if (err.errors[errName].message) {
				return err.errors[errName].message;
			} 
		}
	} else {
		return 'Unknown server error';
	}
};