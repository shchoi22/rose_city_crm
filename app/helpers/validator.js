var mongoose_validate = require('mongoose-validator');

module.exports = {
  name_validator: [
      mongoose_validate({
        validator: 'isLength',
        arguments: [1, 60],
        message: 'Should be between {ARGS[0]} and {ARGS[1]} characters'
      }),
      mongoose_validate({
        validator: 'isAlphanumeric',
        passIfEmpty: false,
        message: 'Name should contain alpha-numeric characters only'
      })
    ],
  
    general_string_short_validator: [
      mongoose_validate({
        validator: 'isLength',
        arguments: [0, 30],
        message: 'Should be between {ARGS[0]} and {ARGS[1]} characters'
      }),
      mongoose_validate({
        validator: 'isAscii',
        passIfEmpty: true,
        message: 'Should contain ascii characters only'
      })],
  
   general_string_long_validator: [
      mongoose_validate({
        validator: 'isLength',
        arguments: [0, 60],
        message: 'Should be between {ARGS[0]} and {ARGS[1]} characters'
      }),
      mongoose_validate({
        validator: 'isAscii',
        passIfEmpty: true,
        message: 'Should contain ascii characters only'
      })],
  
    zipcode_validator: [
      mongoose_validate({
        validator: 'isLength',
        arguments: 5,
        message: 'Zipcode should be 5 characters'
      }),
      mongoose_validate({
        validator: 'isNumeric',
        message: 'Zipcode should contain only digits'
      })
    ],
  
    phone_number_validator: [
      mongoose_validate({
        validator: 'isMobilePhone',
        arguments: 'en-US',
        message: 'Invalid phone number'
      })
    ],
    email_address_validator: [
      mongoose_validate({
        validator: 'isEmail',
        sanitizer: 'normalizeEmail',
        message: 'Invalid email format'
      })
    ],
  
    date_of_birth_validator: [
      mongoose_validate({
        validator: 'isDate',
        message: 'Invalid date for date of birth'
      })
    ]
};