var _ = require('underscore'),
    config = require('../../config/config');

function _calculateAge(birthday) { // birthday is a date
    var ageDifMs = Date.now() - birthday.getTime();
    var ageDate = new Date(ageDifMs); // miliseconds from epoch
    return Math.abs(ageDate.getUTCFullYear() - 1970);
}

module.exports = function(row) {
  var data = {};

  for(var i in config.member_field) {
    if (row) {
      if (row[config.member_field[i]] && config.member_field[i] == "affiliation") {
        data[config.member_field[i]] = row[config.member_field[i]]["name"];
      } else if (row[config.member_field[i]] && config.member_field[i] == "is_active") {
        data[config.member_field[i]] = ((row[config.member_field[i]] > 0) ? "Y" : "N");
      } else if (row[config.member_field[i]] && config.member_field[i] == "date_of_birth") {
        data[config.member_field[i]] = row[config.member_field[i]].toDateString();
        data["age"] = _calculateAge(row[config.member_field[i]]);
      }
      else {
        data[config.member_field[i]] = row[config.member_field[i]];
      }
      
    }
  }

  //unwinds insurance data
  if (row["insurances"] && row["insurances"].length > 0) {
    for (var i in config.insurance_field) {
      data[config.insurance_field[i]] = row["insurances"][0][config.insurance_field[i]]
    }
  }

  //unwinds contact data
  if (row["contacts"] && row["contacts"].length > 0) {
    for (var i in row["contacts"]) {
      var contact = row["contacts"][i];

      //For Emergency Contacts
      if(contact["contact_type"] === "Emergency") {
        for(var j in config.contact_field) {
          data["emergency_contact_" + config.contact_field[j]] = contact[config.contact_field[j]];
        }
      } 
      // For Alternative Contacts
      else if(contact["contact_type"] === "Alternative") {
        for(var j in config.contact_field) {
          data["alternative_contact_" + config.contact_field[j]] = contact[config.contact_field[j]];
        }
      }
      // For Parent Contacts 
      else if(contact["contact_type"] === "Parent") {
        for(var j in config.parent_contact_field) {
          data["parent_contact_" + config.parent_contact_field[j]] = contact[config.parent_contact_field[j]];
        }
      } else {
        continue;
      }
    }
  }

  return data;
}