var _ = require('underscore');

module.exports = function(a, b) {
	var diff = {};
	for(var i in b) {
		if(!a.hasOwnProperty(i) || !_.isEqual(b[i], a[i])) {
			diff[i] = {
				from: a[i],
				to: b[i]
			};
		}
	}
	return diff;
}