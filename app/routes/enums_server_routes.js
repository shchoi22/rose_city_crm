var enums = require('../controllers/enum_server_controller');

module.exports = function(app) {
	app.route('/api/enums')
	  .get(enums.enumValues);
};