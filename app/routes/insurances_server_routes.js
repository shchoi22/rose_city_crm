var insurances = require('../controllers/insurances_server_controller'),
    members = require('../controllers/members_server_controller');

module.exports = function(app) {
	app.route('/api/members/:memberId/insurances')
	  .get(insurances.list)
	  .post(insurances.create);

	app.route('/api/members/:memberId/insurances/:insuranceId')
	  .get(insurances.read)
	  .put(insurances.update)
	  .delete(insurances.delete);

	app.param('memberId', members.memberById);
	app.param('insuranceId', insurances.insuranceById);
}