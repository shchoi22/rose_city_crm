var team = require('../controllers/team_server_controller');

module.exports = function(app) {
	app.route('/api/teams')
	.get(team.list)
	.post(team.create);

	app.route('/api/teams/:teamId')
	.get(team.read)
	.put(team.update)
	.delete(team.delete);

	app.param('teamId', team.teamById);
}