var notes = require('../controllers/notes_server_controller'),
    members = require('../controllers/members_server_controller');

module.exports = function(app) {
	app.route('/api/members/:memberId/notes')
	  .get(notes.list_for_member)
	  .post(notes.create_att_member);

	app.route('/api/members/:memberId/notes/:noteId')
	  .get(notes.read)
	  .put(notes.update)
	  .delete(notes.delete_att_member);

	app.param('memberId', members.memberById);
	app.param('noteId', notes.noteById);
}