//Sample route
module.exports = function(app) {
	var index = require('../controllers/index_server_controller');
	
	app.get('/*', index.render);
}