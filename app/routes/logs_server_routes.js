var members = require('../controllers/members_server_controller'),
    logs = require('../controllers/logs_server_controller');

module.exports = function(app) {
	app.route('/api/logs/members/:memberId')
	.get(logs.list)

	app.param('memberId', members.memberById);
}