var contacts = require('../controllers/contacts_server_controller'),
    members = require('../controllers/members_server_controller');

module.exports = function(app) {
	app.route('/api/members/:memberId/contacts')
	  .get(contacts.list)
	  .post(contacts.create);

	app.route('/api/members/:memberId/contacts/:contactId')
	  .get(contacts.read)
	  .put(contacts.update)
	  .delete(contacts.delete);

	app.param('memberId', members.memberById);
	app.param('contactId', contacts.contactById);
}