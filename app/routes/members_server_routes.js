var members = require('../controllers/members_server_controller'),
    users = require('../controllers/users_server_controller');

module.exports = function(app) {
	app.route('/api/members')
	.get(members.list)
	.post(members.create);

    app.route('/api/members/download')
	 .get(users.ensureAdmin, members.report);

	app.route('/api/members/:memberId')
	.get(members.read)
	.put(members.update)
	.delete(members.delete);

	app.param('memberId', members.memberById);
}