var path = require('path'),
    users = require('../controllers/users_server_controller'),
    session = require('../controllers/session_server_controller');
//    auth = require('../config/auth');

module.exports = function(app) {
  // User Routes

  app.route('/auth/users').get(users.ensureAdmin, users.list)
    .post(users.create);

  app.route('/auth/users/:userId').get(users.show)
    .put(users.ensureAdmin, users.update);
  app.route('/auth/user_name/:username').get(users.exists);

  // Session Routes
  app.get('/auth/session', session.ensureAuthenticated, session.session);
  app.post('/auth/session', session.login);
  app.del('/auth/session', session.logout);
}