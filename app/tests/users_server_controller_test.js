var app = require('../../server'),
    request = require('supertest'),
    should = require('should'),
    mongoose = require('mongoose'),
    User = mongoose.model('User');

var user;

describe('User / Session Controller Unit Test:', function() {
	beforeEach(function(done) {
		user = new User({
			email: 'test@test.com',
			username: 'Test',
			password: 'password',
			role: 'admin'
		});

		user.save(function() {
			done();
		});

	});

	describe('Test GET Methods', function() {
		//Note: 'Profile' virtual schema is not there, kind of like user_info virtual schema.
		it('Should be able to get user info / profile', function(done) {
			request(app).get('/auth/users/' + user._id)
			  .set('Accept', 'application/json')
			  .expect('Content-Type',/json/)
			  .expect(200)
			  .end(function(err, res) {
			  	should.not.exist(err);
			  	res.body.should.have.property('username', user.username);
			  	done();
			  });
		});

		it('Should not be able to get user info with invalid id', function(done) {
			request(app).get('/auth/users/' + user._id + '1')
			  .set('Accept', 'application/json')
			  .expect('Content-Type',/json/)
			  .expect(404)
			  .end(function(err, res) {
			  	should.exist(err);
			  	done();
			  });
		})

		it('Should be able to validate username existing', function(done) {
			request(app).get('/auth/user_name/' + user.username)
			  .set('Accept', 'application/json')
			  .expect('Content-Type', /json/)
			  .expect(200)
			  .end(function(err, res) {
			  	should.not.exist(err);
			  	res.body.exists.should.equal(true);
			  	done();
			  });
		});

		it('Should be able to validate username not existing', function(done) {
			request(app).get('/auth/user_name/' + user.username +'1')
			  .set('Accept', 'application/json')
			  .expect('Content-Type', /json/)
			  .expect(200)
			  .end(function(err, res) {
			  	should.not.exist(err);
			  	res.body.exists.should.equal(false);
			  	done();
			  });
		});
	});

	describe('Test POST/PUT methods', function() {
		it('Should be able to create new user', function(done) {
			body = {
				email: 'test2@test.com',
				username: 'Test2',
				password: 'password'
			};

			request(app).post('/auth/users/')
			  .send(body)
			  .expect('Content-Type', /json/)
			  .expect(200)
			  .end(function(err, res) {
			  	should.not.exist(err);
			  	res.body.should.have.property('_id');
			  	res.body.should.have.property('username', body.username);
			  	res.body.should.have.property('email', body.email);
			  	res.body.should.not.have.property('password');
			  	done();
			  });
		});

		it('Should not be able to create a user with duplicate email', function(done) {
			body = {
				email: 'test@test.com',
				username: 'Test2',
				password: 'password'
			};

			request(app).post('/auth/users/')
			  .send(body)
			  .expect('Content-Type', /json/)
			  .expect(400)
			  .end(function(err, res) {
			  	done();
			  });
		});

		it('Should not be able to create a user with duplicate username', function(done) {
			body = {
				email: 'test2@test.com',
				username: 'Test',
				password: 'password'
			};

			request(app).post('/auth/users/')
			  .send(body)
			  .expect('Content-Type', /json/)
			  .expect(400)
			  .end(function(err, res) {
			  	done();
			  });
		});

		it('Should be able to update username, email, and or roles as an admin only', function(done) {
			reg_user = new User({
				email: 'test2@test.com',
				username: 'Test2',
				password: 'password',
				role: 'user'
			});

			reg_user.save(function(err, data) {
				var agent = request.agent(app);
				agent.post('/auth/session/')
			    .send({
			  	  email: 'test@test.com',
			  	  password: 'password'
			    })
			    .expect('Content-Type', /json/)
			    .expect(200)
			    .end(function(err, res) {
			  	  should.not.exist(err);
			  	  agent.put('/auth/users/' + data._id)
			  	  .send({
			  	  	email: 'test3@testing.com',
			  	  	username: 'Test3',
			  	  	role: 'coach'
			  	  })
			  	  //.expect('Content-Type', /json/)
			  	  //.expect(200)
			  	  .end(function(err,res) {
			  	  	console.log(res.body);
			  	  	res.body.email.should.equal('test3@testing.com');
			  	  	res.body.username.should.equal('Test3');
			  	  	res.body.role.should.equal('coach');
			  	  	done();
			  	  });
			  	});
			});
		});
	});

	describe('Test Session methods', function() {
		it('Should be able to login', function(done) {
			request.agent(app).post('/auth/session/')
			  .send({
			  	email: 'test@test.com',
			  	username: 'Test',
			  	password: 'password'
			  })
			  .expect('Content-Type', /json/)
			  .expect(200)
			  .end(function(err, res) {
			  	should.not.exist(err);
			  	res.body.should.have.property('_id');
			  	res.body.should.have.property('username', user.username);
			  	res.body.should.have.property('email', user.email);
			  	res.body.should.not.have.property('password');
			  	done();
			  });
		});

		it('Should not be able to login with invalid info', function(done) {
			body = {
				email: 'test@test.com',
				username: 'Test',
				password: 'password1'
			}
			request.agent(app).post('/auth/session/')
			  .send(body)
			  .expect('Content-Type', /json/)
			  .expect(400)
			  .end(function(err, res) {
			  	res.body.should.have.property('errors');
			  	done();
			  });
		});

		it('Should be able to access user info when logged in', function(done) {
			var agent = request.agent(app);
			agent.post('/auth/session/')
			  .send({
			  	email: 'test@test.com',
			  	username: 'Test',
			  	password: 'password'
			  })
			  .expect('Content-Type', /json/)
			  .expect(200)
			  .end(function(err, res) {
			  	should.not.exist(err);
			  	agent.get('/auth/session/')
			  	  .expect('Content-Type', /json/)
			  	  .expect(200)
			  	  .end(function(err, res) {
			  	  	should.not.exist(err);
			  	  	res.body.should.have.property('_id');
			  	  	res.body.should.have.property('username', user.username);
			  	  	res.body.should.have.property('email', user.email);
			  	  	res.body.should.not.have.property('password');
			  		done();
			  	  });
			  });
		});

		it('Should be able to logout when logged in', function(done) {
			var agent = request.agent(app);
			agent.post('/auth/session/')
			  .send({
			  	email: 'test@test.com',
			  	username: 'Test',
			  	password: 'password'
			  })
			  .expect(200)
			  .end(function(err, res) {
			  	should.not.exist(err);
			  	agent.del('/auth/session/')
			  	  .expect(200)
			  	  .end(function(err, res) {
			  	  	should.not.exist(err);
			  		done();
			  	  });
			  });
		});


	});

	afterEach(function(done) {
		User.remove(function() {
			done();
		});
	});
});