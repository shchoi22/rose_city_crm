var app = require('../../server'),
    request = require('supertest'),
    should = require('should'),
    mongoose = require('mongoose'),
    Member = mongoose.model('Member'),
    User = mongoose.model('User'),
    Note = mongoose.model('Note');

var member, user, note;

describe('Note Controller Unit Test:', function() {
	beforeEach(function(done) {
		user = new User({
			email: 'test@test.com',
			username: 'test',
			password: 'password'
		});

		member = new Member({
			first_name: 'Test',
			last_name: 'Test',
			member_type: 'Skater',
			home_address: '123 Main Street',
			city: 'Portland',
			state: 'OR',
			zip_code: '97205',
			date_of_birth: '01/31/1990'
		});

		user.save(function() {
			member.save(function() {
				note = new Note({
					created_by: user._id,
					attached_to: member._id,
					content: 'testing'
				});
				note.save(function() {
					member.notes.push(note._id);
					member.save(function(){
						done();
					});
				});
			});
		});
	});

	describe('Test GET Methods', function() {
		it('Should be able to get list of notes for a member', function(done) {
			request(app).get('/api/members/' + member._id + '/notes/')
			  .set('Accept', 'application/json')
			  .expect('Content-Type',/json/)
			  .expect(200)
			  .end(function(err, res) {
			  	should.not.exist(err);
			  	res.body.should.be.an.Array.and.have.lengthOf(1);
			  	res.body[0].should.have.property('attached_to');
			  	res.body[0].should.have.property('created_by');
			  	res.body[0].should.have.property('content', note.content);
			  	done();
			  });
		});

		it('Should be able to get a note for a member', function(done){
			request(app).get('/api/members/' + member._id + '/notes/' + member.notes[0])
			  .set('Accept', 'application/json')
			  .expect('Content-Type',/json/)
			  .expect(200)
			  .end(function(err, res) {
			  	should.not.exist(err);
			  	res.body.should.be.an.Object;
			  	res.body.should.have.property('content', note.content);
			  	done();
			  });
		});

		it('Should not be able to get a note with invalid id', function(done) {
			request(app).get('/api/members/' + member._id + '/notes/' + member.notes[0] + '1')
			  .set('Accept', 'application/json')
			  .expect('Content-Type',/json/)
			  .expect(400)
			  .end(function(err, res) {
			  	should.exist(err);
			  	done();
			  });
		});
	});
    
    describe('Test Post/Put Methods', function() {
    	it('Should be able to create a new note', function(done) {
    		body = {
    		  	attached_to: member._id,
    		  	created_by: user._id,
    		  	content: 'Test2'
    		};

    		request(app).post('/api/members/' + member._id + '/notes/')
    		  .send(body)
    		  .expect('Content-Type', /json/)
    		  .expect(200)
    		  .end(function(err,res) {
    		  	should.not.exist(err);
    		  	request(app).get('/api/members/' + member._id + '/notes/' + res.body.notes[1]._id)
    		  	  .set('Accept', 'application/json')
    		  	  .expect('Content-Type', /json/)
    		      .expect(200)
    		      .end(function(err, res) {
    		      	should.not.exist(err);
			  		res.body.should.have.property('content', body.content);
    		  		done();
    		      });
    		  });
    	});

    	it('Should be able to update an existing note', function(done) {
    		request(app).put('/api/members/' + member._id + '/notes/' + member.notes[0])
    		  .send({
    		  	content: 'Test2'
    		  })
    		  .expect('Content-Type', /json/)
    		  .expect(200)
    		  .end(function(err, res) {
    		  	should.not.exist(err);
    		  	request(app).get('/api/members/' + member._id + '/notes/'+ member.notes[0])
    		  	  .expect(200)
    		  	  .end(function(err, res) {
			  		res.body.should.have.property('content', body.content);
			  		done();
    		  	  });
    		  });
    	});

    	it('Should not be able to update an existing note with invalid id', function(done) {
			request(app).put('/api/members/' + member._id + '/notes/' + member.notes[0]+'1')
    		  .send({
    		  	content: 'Test2'
    		  })
    		  .expect('Content-Type', /json/)
    		  .expect(400)
    		  .end(function(err, res) {
    		  	should.exist(err);
    		  	request(app).get('/api/members/' + member._id + '/notes/'+ member.notes[0])
    		  	  .end(function(err, res) {
			  		res.body.should.have.property('content', note.content);
			  		done();
    		  	  });
    		  });
    	});
    });

	describe('Test Delete Methods', function() {
		it('Should be able to delete existing note', function(done) {
			request(app).delete('/api/members/' + member._id + '/notes/' + member.notes[0])
			  .set('Accept', 'application/json')
			  .expect('Content-Type',/json/)
			  .expect(200)
			  .end(function(err, res) {
			  	should.not.exist(err);
			  	res.body.contacts.should.be.empty;
			  	request(app).get('/api/members/' + member._id)
			  	  .end(function(err, res) {
			  	  	res.body.notes.should.be.empty;
			  	  	done();
			  	  });
			  });
		});

		it('Should not be able to delete existing note with invalid id', function(done) {
			request(app).delete('/api/members/' + member._id + '/notes/' + member.notes[0]+'1')
			  .set('Accept', 'application/json')
			  .expect('Content-Type',/json/)
			  .expect(400)
			  .end(function(err, res) {
			  	should.exist(err);
			  	done();
			  });
		});
	});

	afterEach(function(done) {
		Member.remove(function() {
			User.remove(function() {
				Note.remove(function(){
					done();
				});
			});
		});
	});
});