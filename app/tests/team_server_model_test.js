var app = require('../../server'),
    request = require('supertest'),
    should = require('should'),
    _ = require('underscore'),
    mongoose = require('mongoose'),
    Member = mongoose.model('Member'),
    Team = mongoose.model('Team');

var member, team;

describe('Team Model Unit Tests:', function() {
	
	beforeEach(function(done){
		member = new Member({
			first_name: 'Test',
			last_name: 'Test',
			member_type: 'Skater',
			home_address: '123 Main Street',
			city: 'Portland',
			state: 'OR',
			zip_code: '97205',
			date_of_birth: Date('01/31/1990')
		});

		team = new Team({
			name: 'Test'
		});

		member.save(function() {
			done();
		});

	});

	describe('Testing Team save with Member', function() {
		it('Should be able to save', function(done) {
			team.members.push(member._id);
			team.save(function(err) {
				should.not.exist(err);
				member.affiliation = team._id;
				member.save(function(err) {
					should.not.exist(err);
					done();
				});
			});
		});

		it('Should not be able to save duplicate team', function(done){
			team.save();
			team2 = new Team({
				name: 'Test'
			});

			team2.save(function(err) {
				should.exist(err);
				done();
			});
		});
	});
describe('Team validations', function(){

  it('Should contain 30 characters or less', function(){
    team.name = 'abcdefghijklmnopqrstuvwyzABCDEFGHIJK';
    team.save(function(err){
      should.exist(err);
      done();
    });
  });
  
  it('Should not be able to save without name', function(done) {
			team.name = null;
			team.save(function(err) { 
				should.exist(err);
				done();
			});
		});
});
  
	afterEach(function(done) {
		Team.remove(function() {
			Member.remove(function() {
				done();
			});
		});
	});
});