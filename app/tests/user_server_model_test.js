var app = require('../../server'),
    request = require('supertest'),
    should = require('should'),
    _ = require('underscore'),
    mongoose = require('mongoose'),
    User = mongoose.model('User');

var user;

describe('User Model Unit Tests:', function() {
	
	beforeEach(function(done){
		user = new User({
			email: 'test@test.com',
			username: 'test',
			password: 'test',
			role: 'admin',
		});

		done();
	});

	describe('Testing User save and required fields', function() {
		it('Should be able to save', function() {
			user.save(function(err) {
				should.not.exist(err);
			});
		});

		it('Should not be able to save without email', function() {
			user.email = null;
			user.save(function(err) {
				should.exist(err);
			});
		});

		it('Should not be able to save without username', function() {
			user.username = null;
			user.save(function(err) {
				should.exist(err);
			});
		});

		it('Should not be able to save without password', function() {
			user.password = null;
			user.save(function(err) {
				should.exist(err);
			});
		});

		it('Should not be able to save without role', function() {
			user.role = null;
			user.save(function(err) {
				should.exist(err);
			});
		})
	});

	describe('Testing virtual schemas', function() {
		it('Should have only specified fields', function() {
			user.save(function(err) {
				should(user.user_info).not.have.property('password');
				should(user.user_info).have.property('_id');
				should(user.user_info).have.property('username');
				should(user.user_info).have.property('email');
			});
		});

		it('Should have hashed version of password', function() {
			user.save(function(err) {
				user.should.have.property('hashedPassword');
			});
			User.findOne(user._id).should.not.have.property('password');
		});
	});


	afterEach(function(done) {
		User.remove(function(){
			done();
		});
	});
});