var app = require('../../server'),
    request = require('supertest'),
    should = require('should'),
    _ = require('underscore'),
    mongoose = require('mongoose'),
    Member = mongoose.model('Member'),
    Note = mongoose.model('Note'),
    User = mongoose.model('User');

var member, note;

describe('Note Model Unit Tests:', function() {
	
	beforeEach(function(done){
		member = new Member({
			first_name: 'Test',
			last_name: 'Test',
			member_type: 'Skater',
			home_address: '123 Main Street',
			city: 'Portland',
			state: 'OR',
			zip_code: '97205',
			date_of_birth: Date('01/31/1990')
		});

		user = new User({
			email: 'test@test.com',
			username: 'test',
			password: 'test'
		});

		user.save(function(){
			member.save(function(){

			});
		});

		note = new Note({
			created_by: user._id,
			attached_to: member._id
		});

		done();
	});

	//TODO: Should include more tests on references?
	describe('Testing Note save and required fields', function() {
		it('Should be able to save', function() {
			note.save(function() {
				member.notes.push(note._id);
				member.save(function(err) {
					should.not.exist(err);
				});
			});
		});

		it('Should not be able to save without created_by', function() {
			note.created_by = null;
			note.save(function(err) {
				should.exist(err);
				member.notes.push(note._id);
				member.save(function(err) {
					should.exist(err);
				});
			});
		});
	});

	afterEach(function(done) {
		Member.remove(function(){
			User.remove(function() {
				Note.remove(function(){
					done();
				});
			});
		});
	});
});
