var app = require('../../server'),
    request = require('supertest'),
    should = require('should'),
    _ = require('underscore'),
    mongoose = require('mongoose'),
    Member = mongoose.model('Member'),
    Contact = mongoose.model('Contact');

var member, contact;

describe('Contact Model Unit Tests:', function() {
	
	beforeEach(function(done){
		member = new Member({
			first_name: 'Test',
			last_name: 'Test',
			member_type: 'Skater',
			home_address: '123 Main Street',
			city: 'Portland',
			state: 'OR',
			zip_code: '97205',
			date_of_birth: '01/31/1990'
		});

		contact = new Contact({
			first_name: 'Test2',
			last_name: 'Test2',
			contact_type: 'Alternative',
			phone_number: '1234567890'
		});

		done();
	});

	describe('Testing Contact save and required fields', function() {
		it('Should be able to save', function() {
			member.contacts.push(contact);
			member.save(function(err) {
				should.not.exist(err);
			});
		});

		it('Should not be able to save without first_name', function() {
			contact.first_name = null;
			member.contacts.push(contact);
			member.save(function(err) {
				should.exist(err);
			});
		});

		it('Should not be able to save without last_name', function() {
			contact.last_name = null;
			member.contacts.push(contact);
			member.save(function(err){
				should.exist(err);
			});
		});

		it('Should not be able to save without contact_type', function() {
			contact.contact_type = null;
			member.contacts.push(contact);
			member.save(function(err) {
				should.exist(err);
			});
		});

		it('Should not be able to save without phone_number', function() {
			contact.phone_number = null;
			member.contacts.push(contact);
			member.save(function(err){
				should.exist(err);
			});
		});
	});

	describe('Testing Contact validation', function() {
		it('Should not be able to save without valid contact_type', function() {
			contact.contact_type = 'Testing';
			member.contacts.push(contact);
			member.save(function(err) {
				should.exist(err);
			});
		});
    
    // validate email
    it('Should not allow an invalid email format', function(){
      contact.email = 'foo@bar';
      member.contacts.push(contact);
      member.save(function(err){
        should.exist(err);
      });
    });
    
    it('Should allow a valid email', function(){
      contact.email = 'foo@bar.com';
      member.contacts.push(contact);
      member.save(function(err){
        should.not.exist(err);
      });
    });
    
    // validate first_name
    it('Should not allow non-alpha characters', function(){
      contact.first_name = 'Bob123';
      member.contacts.push(contact);
      member.save(function(err){
        should.exist(err);
      });
    });
    
     // validate last_name
    it('Should not allow non-alpha characters', function(){
      contact.last_name = '#Smith';
      member.contacts.push(contact);
      member.save(function(err){
        should.exist(err);
      });
    });
    
    // validate phone number
    it('Should allow valid phone number formats', function(){
      contact.phone_number = '5038675309';
      member.contacts.push(contact);
      member.save(function(err){
        should.not.exist(err);
      });
    });
    
    it('Should not allow invalid phone number formats', function(){
      contact.phone_number = '5038675309';
      member.contacts.push(contact);
      member.save(function(err){
        should.exist(err);
      });
    });
    
    // validate zipcode
    it('Should not allow an invalid zipcode', function(){
      contact.zipcode = '1234';
      member.contacts.push(contact);
      member.save(function(err){
        should.exist(err);
      });
    });
  });
  
  afterEach(function(done) {
		Member.remove(function(){
			Contact.remove(function() {
				done();
			});
		});
	});
});

