var app = require('../../server'),
    request = require('supertest'),
    should = require('should'),
    _ = require('underscore'),
    mongoose = require('mongoose'),
    Member = mongoose.model('Member');

var member;

describe('Member Model Unit Tests:', function () {

	beforeEach(function(done) {
		member = new Member({
			first_name: 'Test',
			last_name: 'Test',
			member_type: 'Skater',
			home_address: '123 Main Street',
			city: 'Portland',
			state: 'OR',
			zip_code: '97205',
			date_of_birth: Date('01/31/1990')
		});

		member.save(function(){
			done();
		});
	});

	describe('Testing Member basic save and required fields', function() {
		it('Should be able to save', function() {
			member.save(function(err) {
				should.not.exist(err);
			});
		});

		it('Should not be able to save without first_name', function() {
			member.first_name = null;
			member.validate(function(err) {
				should.exist(err);
			});
		});

		it('Should not be able to save without last_name', function() {
			member.last_name = null;
			member.validate(function(err) {
				should.exist(err);
			});
		});

		it('Should not be able to save without member_type', function() {
			member.member_type = null;

			member.validate(function(err) {
				should.exist(err);
			});
		});
	});


	describe('Testing validations', function() {
		it('Should be able to save with valid values', function() {
			member = _.extend(member, {
				gender: 'Male',
				shirt_size: 'XS',
				skate_size: 'a - 1',
				knee_pad_size: 'a - junior',
				elbow_pad_size: 'a - junior',
				wrist_size: 'junior',
				helmet_size: 'k - small'
			});
			member.save(function(err) {
				should.not.exist(err);
			});
		});

		//TODO: Add tests for each enum fields?
		it('Should not be able to save with invalid values', function() {
			member = _.extend(member, {
				gender: 'Test'
			});
			member.save(function(err) {
				should.exist(err);
			});
		});
    
    // validate zipcode
    it('Should be able to save without zip code', function(){
      member.zip_code = null;
      member.save(function(err){
        should.exist(err);
      });
    });
    
    // validate phone number
    it('Should not allow a phone number with an invalid area code',
      function(){
        member = _.extend(member, {
          phone_number: '0008675309'
        });
        member.save(function(err){
          should.exist(err);
        });
    });
    
    it('Should not allow a phone number with a length more or less than 10',
      function(){
        member = _.extend(member, {
          phone_number: '12345'
        });
        member.save(function(err){
          should.exist(err);
        });
    });
    
    // validate email
		it('Should not allow an email with an invalid format', function(){
      member = _.extend(member, {
        email_address: '@invalid'
      });
      member.save(function(err){
        should.exist(err);
      });
    });
		
		// validate names
    it('Should not allow invalid characters for names', function(){
      member = _.extend(member, {
        first_name: 'Alice#666'
      });
      member.save(function(err){
        should.exist(err);
      });
    });
		
    it('Should not allow more than 60 characters for names', function(){
      member = _.extend(member, {
        last_name: 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
      });
      member.save(function(err){
        should.exist(err);
      });
    });
    
    // validate skate_name
    it('Should allow non-alpha characters for skate_name', function(){
      member = _.extend(member, {
        skate_name: 'Pun ~N~ 10 dead.'
      });
      member.save(function(err){
        if(err) throw err;
      });
    });
		
		// validate birth date
		it('Should require a birthdate', function(){
			member.date_of_birth = null;
			member.save(function(err){
				should.exist(err);
			});
		});
     
	});

	afterEach(function(done){
		Member.remove({_id: member._id},function(){
			done();
		});
	});
});

