var app = require('../../server'),
    request = require('supertest'),
    should = require('should'),
    _ = require('underscore'),
    mongoose = require('mongoose'),
    Member = mongoose.model('Member'),
    Insurance = mongoose.model('Insurance');

var member, insurance;

describe('Insurance Model Unit Tests:', function() {
	
	beforeEach(function(done){
		member = new Member({
			first_name: 'Test',
			last_name: 'Test',
			member_type: 'Skater',
			home_address: '123 Main Street',
			city: 'Portland',
			state: 'OR',
			zip_code: '97205',
			date_of_birth: '01/31/1990'
		});

		insurance = new Insurance({
			WFTDA: 12345,
			WFTDA_confidentiality_acknowledgement_tracking: true,
			concussion_training_complete: false
		});

		done();
	});

	describe('Testing Insurance save and required fields', function() {
		it('Should be able to save', function() {
			member.insurances.push(insurance);
			member.save(function(err) {
				should.not.exist(err);
			});
		});

		it('Should not be able to save without WFTDA', function() {
			insurance.WFTDA = null;
			member.insurances.push(insurance);
			member.save(function(err) {
				should.exist(err);
			});
		});

		it('Should not be able to save without WFTDA_confidentiality_acknowledgement_tracking', function() {
			insurance.WFTDA_confidentiality_acknowledgement_tracking = null;
			member.insurances.push(insurance);
			member.save(function(err){
				should.exist(err);
			});
		});

		it('Should not be able to save without concussion_training_complete', function() {
			insurance.concussion_training_complete = null;
			member.insurances.push(insurance);
			member.save(function(err) {
				should.exist(err);
			});
		});
	});

	afterEach(function(done) {
		Member.remove(function(){
			Insurance.remove(function() {
				done();
			});
		});
	});
});
