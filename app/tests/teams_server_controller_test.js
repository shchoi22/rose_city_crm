var app = require('../../server'),
    request = require('supertest'),
    should = require('should'),
    mongoose = require('mongoose'),
    Member = mongoose.model('Member'),
    Team = mongoose.model('Team');

var member, team;

describe('Team Controller Unit Test:', function() {
	beforeEach(function(done) {
		member = new Member({
			first_name: 'Test',
			last_name: 'Test',
			member_type: 'Skater',
			home_address: '123 Main Street',
			city: 'Portland',
			state: 'OR',
			zip_code: '97205',
			date_of_birth: '01/31/1990'
		});

		team = new Team({
			name: 'Test'
		});

		team.save(function() {
			done();
		});

	});

	describe('Test GET Methods', function() {
		it('Should be able to get list of teams', function(done) {
			request(app).get('/api/teams')
			  .set('Accept', 'application/json')
			  .expect('Content-Type',/json/)
			  .expect(200)
			  .end(function(err, res) {
			  	should.not.exist(err);
			  	res.body.should.be.an.Array.and.have.lengthOf(1);
			  	res.body[0].should.have.property('name', team.name);
			  	done();
			  });
		});

		it('Should be able to get a team', function(done) {
			request(app).get('/api/teams/' + team._id)
			  .set('Accept', 'application/json')
			  .expect('Content-Type', /json/)
			  .expect(200)
			  .end(function(err, res) {
			  	should.not.exist(err);
			  	res.body.should.be.an.Object;
			  	res.body.should.have.property('name', team.name);
			  	done();
			  });

		});

		it('Should not be able to get a team with invalid id', function(done) {
			request(app).get('/api/teams/' + team._id +'1')
			  .set('Accept', 'application/json')
			  .expect('Content-Type', /json/)
			  .expect(500)
			  .end(function(err, res) {
			  	should.exist(err);
			  	done();
			  });
		});
	});

	describe('Test POST/PUT methods', function() {
		it('Should be able to create new team', function(done) {
			body = {
				name: 'Test2'
			};

			request(app).post('/api/teams/')
			  .send(body)
			  .expect('Content-Type', /json/)
			  .expect(200)
			  .end(function(err, res) {
			  	should.not.exist(err);
			  	res.body.name.should.equal('Test2');
			  	done();
			  });
		});

		it('Should not be able to create a team with duplicate name', function(done) {
			var body= {
				name: 'Test'
			};

			request(app).post('/api/teams/')
			  .send(body)
			  .expect('Content-Type', /json/)
			  .expect(400)
			  .end(function(err, res) {
			  	done();
			  });
		});

		it('Should be able to update teams', function(done) {
			member.save();
			var body = {
				name: 'Test5',
				members:[member._id]
			}

			request(app).put('/api/teams/' + team._id)
			  .send(body)
			  .expect('Content-Type', /json/)
			  .expect(200)
			  .end(function(err, res) {
			  	should.not.exist(err);
			  	res.body.name.should.equal('Test5');
			  	res.body.members.should.not.be.empty;
			  	done();
			  });
		});
	});

	describe('Test Delete method', function() {
		it('Should be able to delete a team', function(done) {
			request(app).delete('/api/teams/' + team._id)
			  .set('Accept', 'application/json')
			  .expect('Content-Type', /json/)
			  .expect(200)
			  .end(function(err, res) {
			  	should.not.exist(err);
			  	request(app).get('/api/teams/' + team._id)
			  	  .expect(400)
			  	  .end(function(err, res) {
			  	  	should.exist(err);
			  	  	done();
			  	  });
			  });
		});

		it('Should remove team id reference from associated member', function(done) {
			team.members.push(member._id);
			team.save(function() {
				member.affiliation = team._id;
				member.save();
			});
			request(app).delete('/api/teams/' + team._id)
			  .set('Accept', 'application/json')
			  .expect('Content-Type', /json/)
			  .expect(200)
			  .end(function(err, res) {
			  	request(app).get('/api/members/' + member._id)
			  	  .set('Accept', 'application/json')
			      .expect('Content-Type', /json/)
			  	  .expect(200)
			  	  .end(function(err, res) {
			  	  	should.equal(res.body.affiliation, null);
			  	  	done();
			  	  });
			  });
		});

		it('Should not be able to delete a not existing team', function(done) {
			request(app).delete('/api/teams/' + team._id + '1')
			  .set('Accept', 'application/json')
			  .expect('Content-Type', /json/)
			  .expect(400)
			  .end(function(err, res) {
			  	should.exist(err);
			  	done();
			  });
		});
	});

	afterEach(function(done) {
		Team.remove(function() {
			Member.remove(function(){
				done();
			});
		});
	});
});