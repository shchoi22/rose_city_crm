var app = require('../../server'),
    request = require('supertest'),
    should = require('should'),
    mongoose = require('mongoose'),
    Member = mongoose.model('Member'),
    Contact = mongoose.model('Contact');

var member, contact;

describe('Contact Controller Unit Test:', function() {
	beforeEach(function(done) {
		member = new Member({
			first_name: 'Test',
			last_name: 'Test',
			member_type: 'Skater',
			home_address: '123 Main Street',
			city: 'Portland',
			state: 'OR',
			zip_code: '97205',
			date_of_birth: '01/31/1990'
		});

		contact = new Contact({
			first_name: 'Test',
			last_name: 'Test',
			contact_type: 'Alternative',
			phone_number: '3122187405'
		});

		member.contacts.push(contact);
		member.save(function() {
			done();
		});
	});

	describe('Test GET Methods', function() {
		it('Should be able to get list of contacts for a member', function(done) {
			request(app).get('/api/members/' + member._id + '/contacts/')
			  .set('Accept', 'application/json')
			  .expect('Content-Type',/json/)
			  .expect(200)
			  .end(function(err, res) {
			  	should.not.exist(err);
			  	res.body.should.be.an.Array.and.have.lengthOf(1);
			  	res.body[0].should.have.property('_id');
			  	res.body[0].should.have.property('first_name', contact.first_name);
			  	res.body[0].should.have.property('last_name', contact.last_name);
			  	res.body[0].should.have.property('contact_type', contact.contact_type);
			  	res.body[0].should.have.property('phone_number', contact.phone_number);
			  	done();
			  });
		});

		it('Should be able to get a contact', function(done){
			request(app).get('/api/members/' + member._id + '/contacts/' + member.contacts[0]._id)
			  .set('Accept', 'application/json')
			  .expect('Content-Type',/json/)
			  .expect(200)
			  .end(function(err, res) {
			  	should.not.exist(err);
			  	res.body.should.be.an.Object;
			  	res.body.should.have.property('first_name', contact.first_name);
			  	res.body.should.have.property('last_name', contact.last_name);
			    res.body.should.have.property('contact_type', contact.contact_type);
			  	res.body.should.have.property('phone_number', contact.phone_number);
			  	done();
			  });
		});

		it('Should not be able to get a contact with invalid id', function(done) {
			request(app).get('/api/members/' + member._id + '/contacts/' + member.contacts[0]._id + '1')
			  .set('Accept', 'application/json')
			  .expect('Content-Type',/json/)
			  .expect(400)
			  .end(function(err, res) {
			  	should.exist(err);
			  	done();
			  });
		});
	});
    
    describe('Test Post/Put Methods', function() {
    	it('Should be able to create a new contact', function(done) {
    		body = {
    		  	first_name: 'Test2',
    		  	last_name: 'Test2',
    		  	contact_type: 'Emergency',
    		  	phone_number: '9876543210'
    		};

    		request(app).post('/api/members/' + member._id + '/contacts')
    		  .send(body)
    		  .expect('Content-Type', /json/)
    		  .expect(200)
    		  .end(function(err,res) {
    		  	should.not.exist(err);
    		  	res.body.contacts.should.not.be.empty;
    		  	res.body.contacts[1].should.have.property('first_name', body.first_name);
    		  	res.body.contacts[1].should.have.property('last_name', body.last_name);
    		  	res.body.contacts[1].should.have.property('contact_type', body.contact_type);
    		  	res.body.contacts[1].should.have.property('phone_number', body.phone_number);
    		  	done();
    		  });
    	});

    	it('Should be able to update an existing contact', function(done) {
    		request(app).put('/api/members/' + member._id + '/contacts/' + member.contacts[0]._id)
    		  .send({
    		  	first_name:'Test2',
    		  	last_name: 'Test2',
    		  	contact_type: 'Emergency',
    		  	phone_number: '9876543210'
    		  })
    		  .expect('Content-Type', /json/)
    		  .expect(200)
    		  .end(function(err, res) {
    		  	should.not.exist(err);
    		  	res.body.contacts[0].should.have.property('first_name', 'Test2');
    		  	res.body.contacts[0].should.have.property('last_name', 'Test2');
    		  	res.body.contacts[0].should.have.property('contact_type', 'Emergency');
    		  	res.body.contacts[0].should.have.property('phone_number', '9876543210');
    		  	done();
    		  });
    	});

    	it('Should not be able to update an existing contact with invalid id', function(done) {
			request(app).put('/api/members/' + member._id + '/contacts/' + member.contacts[0]._id+'1')
    		  .send({
    		  	first_name:'Test2',
    		  	last_name: 'Test2',
    		  	contact_type: 'Emergency',
    		  	phone_number: '9876543210'
    		  })
    		  .expect('Content-Type', /json/)
    		  .expect(400)
    		  .end(function(err, res) {
    		  	should.exist(err);
    		  	request(app).get('/api/members/' + member._id + '/contacts/'+ member.contacts[0]._id)
    		  	  .end(function(err, res) {
    		  	  	res.body.should.have.property('first_name', contact.first_name);
			  		res.body.should.have.property('last_name', contact.last_name);
			    	res.body.should.have.property('contact_type', contact.contact_type);
			  		res.body.should.have.property('phone_number', contact.phone_number);
			  		done();
    		  	  });
    		  });
    	});
    });

	describe('Test Delete Methods', function() {
		it('Should be able to delete existing contact', function(done) {
			request(app).delete('/api/members/' + member._id + '/contacts/' + member.contacts[0]._id)
			  .set('Accept', 'application/json')
			  .expect('Content-Type',/json/)
			  .expect(200)
			  .end(function(err, res) {
			  	should.not.exist(err);
			  	res.body.contacts.should.be.empty;
			  	request(app).get('/api/members/' + member._id)
			  	  .end(function(err, res) {
			  	  	res.body.contacts.should.be.empty;
			  	  	done();
			  	  });
			  });
		});

		it('Should not be able to delete existing contact with invalid id', function(done) {
			request(app).delete('/api/members/' + member._id + '/contacts/' + member.contacts[0]._id +'1')
			  .set('Accept', 'application/json')
			  .expect('Content-Type',/json/)
			  .expect(400)
			  .end(function(err, res) {
			  	should.exist(err);
			  	done();
			  });
		});
	});

	afterEach(function(done) {
		Member.remove(function() {
			Contact.remove(function(){
				done();
			});
		});
	});
});