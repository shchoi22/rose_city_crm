var app = require('../../server'),
    request = require('supertest'),
    should = require('should'),
    mongoose = require('mongoose'),
    Member = mongoose.model('Member'),
    Insurance = mongoose.model('Insurance');

var member, insurance;

describe('Insurance Controller Unit Test:', function() {
	beforeEach(function(done) {
		member = new Member({
			first_name: 'Test',
			last_name: 'Test',
			member_type: 'Skater',
			home_address: '123 Main Street',
			city: 'Portland',
			state: 'OR',
			zip_code: '97205',
			date_of_birth: '01/31/1990'
		});

		insurance = new Insurance({
			WFTDA: 1234567890,
			WFTDA_confidentiality_acknowledgement_tracking: true
		});

		member.insurances.push(insurance);
		member.save(function() {
			done();
		});
	});

	describe('Test GET Methods', function() {
		it('Should be able to get list of insurances for a member', function(done) {
			request(app).get('/api/members/' + member._id + '/insurances/')
			  .set('Accept', 'application/json')
			  .expect('Content-Type', /json/)
			  .expect(200)
			  .end(function(err, res) {
			  	should.not.exist(err);
			  	res.body.should.be.an.Array.and.have.lengthOf(1);
			  	res.body[0].should.have.property('_id');
			  	res.body[0].should.have.property('WFTDA', insurance.WFTDA);
			  	done();
			  });
		});

		it('Should be able to get an insurance', function(done){
			request(app).get('/api/members/' + member._id + '/insurances/' + member.insurances[0]._id)
			  .set('Accept', 'application/json')
			  .expect('Content-Type',/json/)
			  .expect(200)
			  .end(function(err, res) {
			  	should.not.exist(err);
			  	res.body.should.be.an.Object;
			  	res.body.should.have.property('WFTDA', insurance.WFTDA);
			  	done();
			  });
		});

		it('Should not be able to get an insurance with invalid id', function(done) {
			request(app).get('/api/members/' + member._id + '/insurances/' + member.insurances[0]._id + '1')
			  .set('Accept', 'application/json')
			  .expect('Content-Type',/json/)
			  .expect(400)
			  .end(function(err, res) {
			  	should.exist(err);
			  	done();
			  });
		});
	});
    
    describe('Test Post/Put Methods', function() {
    	it('Should be able to create a new insurance record', function(done) {
    		body = {
    		  	WFTDA: 9876543210,
    		  	WFTDA_confidentiality_acknowledgement_tracking: false
    		};

    		request(app).post('/api/members/' + member._id + '/insurances/')
    		  .send(body)
    		  .expect('Content-Type', /json/)
    		  .expect(200)
    		  .end(function(err,res) {
    		  	should.not.exist(err);
    		  	res.body.insurances.should.not.be.empty;
    		  	res.body.insurances[1].should.have.property('WFTDA', body.WFTDA);
    		  	done();
    		  });
    	});

    	it('Should be able to update an existing insurance', function(done) {
    		request(app).put('/api/members/' + member._id + '/insurances/' + member.insurances[0]._id)
    		  .send({
    		  	WFTDA:9876543210,
    		  	WFTDA_confidentiality_acknowledgement_tracking: false
    		  })
    		  .expect('Content-Type', /json/)
    		  .expect(200)
    		  .end(function(err, res) {
    		  	should.not.exist(err);
    		  	res.body.insurances[0].should.have.property('WFTDA', 9876543210);
    		  	done();
    		  });
    	});

    	it('Should not be able to update an existing insurance with invalid id', function(done) {
			request(app).put('/api/members/' + member._id + '/insurances/' + member.insurances[0]._id+'1')
    		  .send({
    		  	WFTDA:9876543210,
    		  	WFTDA_confidentiality_acknowledgement_tracking:false
    		  })
    		  .expect('Content-Type', /json/)
    		  .expect(400)
    		  .end(function(err, res) {
    		  	should.exist(err);
    		  	request(app).get('/api/members/' + member._id + '/insurances/'+ member.insurances[0]._id)
    		  	  .end(function(err, res) {
    		  	  	res.body.should.have.property('WFTDA', insurance.WFTDA);
			  		done();
    		  	  });
    		  });
    	});
    });

	describe('Test Delete Methods', function() {
		it('Should be able to delete existing insurance', function(done) {
			request(app).delete('/api/members/' + member._id + '/insurances/' + member.insurances[0]._id)
			  .set('Accept', 'application/json')
			  .expect('Content-Type',/json/)
			  .expect(200)
			  .end(function(err, res) {
			  	should.not.exist(err);
			  	res.body.insurances.should.be.empty;
			  	request(app).get('/api/members/' + member._id)
			  	  .end(function(err, res) {
			  	  	res.body.insurances.should.be.empty;
			  	  	done();
			  	  });
			  });
		});

		it('Should not be able to delete existing insurance with invalid id', function(done) {
			request(app).delete('/api/members/' + member._id + '/insurances/' + member.insurances[0]._id +'1')
			  .set('Accept', 'application/json')
			  .expect('Content-Type',/json/)
			  .expect(400)
			  .end(function(err, res) {
			  	should.exist(err);
			  	done();
			  });
		});
	});

	afterEach(function(done) {
		Member.remove(function() {
			Insurance.remove(function(){
				done();
			});
		});
	});
});