var app = require('../../server'),
    request = require('supertest'),
    should = require('should'),
    mongoose = require('mongoose'),
    Member = mongoose.model('Member'),
    Contact = mongoose.model('Contact');
    
describe('Enum Controller Unit Test:', function() {

	describe('Test GET Methods', function() {
		it('Should be able to get list of member and contact field enum values', function(done) {
			request(app).get('/api/enums/')
			  .set('Accept', 'application/json')
			  .expect('Content-Type',/json/)
			  .expect(200)
			  .end(function(err, res) {
			  	should.not.exist(err);
			  	res.body.members.member_type.should.be.eql(Member.schema.path('member_type').enumValues);
			  	res.body.members.gender.should.be.eql(Member.schema.path('gender').enumValues);
			  	res.body.members.helmet_size.should.be.eql(Member.schema.path('helmet_size').enumValues);
			  	res.body.contacts.contact_type.should.be.eql(Contact.schema.path('contact_type').enumValues);
			  	done();
			  });
		});
	});
});