var app = require('../../server'),
    request = require('supertest'),
    should = require('should'),
    mongoose = require('mongoose'),
    Member = mongoose.model('Member'),
    Contact = mongoose.model('Contact'),
    Note = mongoose.model('Note'),
    Team = mongoose.model('Team'),
    User = mongoose.model('User');

var member, contact, note, team, user, agent;

describe('Member Controller Unit Test:', function() {
	beforeEach(function(done) {
		member = new Member({
			first_name: 'Test',
			last_name: 'Test',
			member_type: 'Skater',
			home_address: '123 Main Street',
			city: 'Portland',
			state: 'OR',
			zip_code: '97205',
			date_of_birth: '01/31/1990'
		});

		user = new User({
			email: 'test@test.com',
			username: 'Test',
			password: 'password'
		});

		team = new Team({
			name: 'Test'
		});

		member.save(function() {
			user.save(function() {
				agent = request.agent(app);
				agent.post('/auth/session/')
				.send({
					email: 'test@test.com',
					password: 'password'
				})
				.expect('Content-Type', /json/)
				.expect(200)
				.end(function(err,res) {
					done();
				});
			});
		});
	});

	describe('Test GET Methods', function() {
		it('Should be able to get list of members', function(done) {
			agent.get('/api/members')
			  .set('Accept', 'application/json')
			  .expect('Content-Type',/json/)
			  .expect(200)
			  .end(function(err, res) {
			  	should.not.exist(err);
			  	res.body.should.be.an.Array.and.have.lengthOf(1);
			  	res.body[0].should.have.property('first_name', member.first_name);
			  	res.body[0].should.have.property('last_name', member.last_name);
			  	res.body[0].should.have.property('member_type', member.member_type);
					res.body[0].should.have.property('home_address', member.home_address);
					res.body[0].should.have.property('city', member.city);
					res.body[0].should.have.property('state', member.state);
					res.body[0].should.have.property('zip_code', member.zip_code);
					//res.body[0].should.have.property('date_of_birth', member.date_of_birth);
			  	done();
			  });

		});

		it('Should be able to get a member', function(done) {
			agent.get('/api/members/' + member._id)
			  .set('Accept', 'application/json')
			  .expect('Content-Type', /json/)
			  .expect(200)
			  .end(function(err, res) {
			  	should.not.exist(err);
			  	res.body.should.be.an.Object;
			  	res.body.should.have.property('first_name', member.first_name);
			  	res.body.should.have.property('last_name', member.last_name);
			  	res.body.should.have.property('member_type', member.member_type);
					res.body.should.have.property('home_address', member.home_address);
					res.body.should.have.property('city', member.city);
					res.body.should.have.property('state', member.state);
					res.body.should.have.property('zip_code', member.zip_code);
					//res.body.should.have.property('date_of_birth', member.date_of_birth);
			  	done();
			  });

		});

		it('Should not be able to get a member with invalid id', function(done) {
			agent.get('/api/members/' + member._id +'1')
			  .set('Accept', 'application/json')
			  .expect('Content-Type', /json/)
			  .expect(500)
			  .end(function(err, res) {
			  	should.exist(err);
			  	done();
			  });
		});

		it('Should be able to search for member using first name, last name or skate name', function(done) {
			var member2 = new Member({
				first_name: 'SuperTest',
				last_name: 'SuperTest',
				member_type: 'Skater',
				skate_name: 'Awesome',
				home_address: '456 Mockingbird Lane',
				city: 'Beaverton',
				state: 'OR',
				zip_code: '97006',
				date_of_birth: '12/12/1985',
				
			});
			member2.save();
			agent.get('/api/members?search_text=' + member2.first_name)
			  .expect('Content-Type', /json/)
			  .expect(200)
			  .end(function(err, res) {
			  	should.not.exist(err);
			  	res.body.should.be.an.Array.and.have.lengthOf(1);
			  	res.body[0].last_name.should.equal('SuperTest');
			  	res.body[0].skate_name.should.equal('Awesome');
			  });

			agent.get('/api/members?search_text=' + member2.last_name)
			  .expect('Content-Type', /json/)
			  .expect(200)
			  .end(function(err, res) {
			  	should.not.exist(err);
			  	res.body.should.be.an.Array.and.have.lengthOf(1);
			  	res.body[0].first_name.should.equal('SuperTest');
			  	res.body[0].skate_name.should.equal('Awesome');
			  });

			agent.get('/api/members?search_text=' + member2.skate_name)
			  .expect('Content-Type', /json/)
			  .expect(200)
			  .end(function(err, res) {
			  	should.not.exist(err);
			  	res.body.should.be.an.Array.and.have.lengthOf(1);
			  	res.body[0].first_name.should.equal('SuperTest');
			  	res.body[0].last_name.should.equal('SuperTest');
			  	done();
			  });
		});
	});

	describe('Test POST/PUT methods', function() {
		it('Should be able to create new member', function(done) {
			var member2;

			team.save(function(error, data) {
				body = {
					first_name: 'Test1',
					last_name: 'Test1',
					member_type:'Skater',
					affiliation: data._id,
					home_address: '456 Mockingbird Lane',
					city: 'Beaverton',
					state: 'OR',
					zip_code: '97006',
					date_of_birth: '12/12/1985',
				};

				agent.post('/api/members/')
			 	.send(body)
			 	.expect('Content-Type', /json/)
			 	.expect(200)
			 	.end(function(err, res) {
			  		should.not.exist(err);
			  		res.body.should.have.property('_id');
			  		res.body.first_name.should.equal('Test1');
			  		res.body.last_name.should.equal('Test1');
			  		res.body.member_type.should.equal('Skater');
						
			  		member2 = res.body;

			  		agent.get('/api/teams/' + team._id)
			  		  .set('Accept', 'application/json')
			  		  .expect('Content-Type', /json/)
			  		  .expect(200)
			  		  .end(function(err, res) {
			  			should.not.exist(err);
			  			res.body.members[0]._id.should.equal(member2._id);
			  			done();
			  		});
			 	});
			});
			
		});

		it('Should be able to update members', function(done) {

			member.first_name = 'Test2';
			member.last_name = 'Test2';
			member.member_type = 'Alumni';

			agent.put('/api/members/' + member._id)
			  .send(member)
			  .expect('Content-Type', /json/)
			  .expect(200)
			  .end(function(err, res) {
			  	should.not.exist(err);
			  	res.body.should.have.property('_id');
			  	res.body.first_name.should.equal('Test2');
			  	res.body.last_name.should.equal('Test2');
			  	res.body.member_type.should.equal('Alumni');
			  	done();
			  });
		});
	});

	describe('Test Delete method', function() {
		it('Should be able to delete a member', function(done) {
			agent.delete('/api/members/' + member._id)
			  .set('Accept', 'application/json')
			  .expect('Content-Type', /json/)
			  .expect(200)
			  .end(function(err, res) {
			  	should.not.exist(err);
			  	res.body.should.have.property('_id');
			  	res.body.first_name.should.equal('Test');
			  	res.body.last_name.should.equal('Test');
			  	res.body.member_type.should.equal('Skater');
			  	agent.get('/api/members/' + member._id)
			  	  .expect(400)
			  	  .end(function(err, res) {
			  	  	should.exist(err);
			  	  	done();
			  	  });
			  });
		});

		it('Should remove member id reference from associated team', function(done) {
			team.members.push(member._id);
			team.save(function() {
				member.affiliation = team._id;
				member.save();
			});
			agent.delete('/api/members/' + member._id)
			  .set('Accept', 'application/json')
			  .expect('Content-Type', /json/)
			  .expect(200)
			  .end(function(err, res) {
			  	agent.get('/api/teams/' + team._id)
			  	  .expect(200)
			  	  .end(function(err, res) {
			  	  	res.body.members.should.be.empty
			  	  	done();
			  	  });
			  });
		});

		it('Should not be able to delete a not existing member', function(done) {
			agent.delete('/api/members/' + member._id + '1')
			  .set('Accept', 'application/json')
			  .expect('Content-Type', /json/)
			  .expect(400)
			  .end(function(err, res) {
			  	should.exist(err);
			  	done();
			  });
		});
	});

	afterEach(function(done) {
		Member.remove(function() {
			Team.remove(function(){
				User.remove(function() {
					done();
				});
			});
		});
	});
});