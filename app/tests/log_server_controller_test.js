var app = require('../../server'),
    request = require('supertest'),
    should = require('should'),
    mongoose = require('mongoose'),
    Member = mongoose.model('Member'),
    User = mongoose.model('User'),
    Log = mongoose.model('Log');

var member, log, user;

describe('Log Controller Unit Test:', function() {
	beforeEach(function(done) {
		member = new Member({
			first_name: 'Test',
			last_name: 'Test',
			member_type: 'Skater',
			home_address: '123 Main Street',
			city: 'Portland',
			state: 'OR',
			zip_code: '97205',
			date_of_birth: '01/31/1990'
		});

		user = new User({
			email: 'test@test.com',
			username: 'Test',
			password: 'password'
		});

		user.save(function() {
			member.save(function() {
				done();
			});
		});	
	});

	//TODO: Add more tests for create and delete of members and its logs

	describe('Test GET Methods', function() {
		it('Should be able to get list of logs for a member', function(done) {
			var agent = request.agent(app);
			agent.post('/auth/session/')
			  .send({
			  	email: 'test@test.com',
			  	password: 'password'
			  })
			  .expect('Content-Type', /json/)
			  .expect(200)
			  .end(function(err, res) {
			  	agent.put('/api/members/' + member._id)
			      .send({
			      	first_name: 'SuperTest'
			      })
			  	  .expect('Content-Type',/json/)
			      .expect(200)
			      .end(function(err, res) {
			  		agent.get('/api/logs/members/' + member._id)
			  	  	.expect('Content-Type', /json/)
			  	  	.expect(200)
			  	  	.end(function(err, res){
			  	  		should.not.exist(err);
			  	  		res.body.should.be.an.Array.and.have.lengthOf(1);
			  	  		res.body[0].diff.should.have.property('first_name');
			  	  		res.body[0].diff.should.not.have.property('last_name');
			  			done();
			  	  	});
			  	});
			});
		});
	});

	afterEach(function(done) {
		Member.remove(function() {
			User.remove(function() {
				done();
			})
			
		});
	});
});