var mongoose = require("mongoose"),
    validator = require('../helpers/validator'),
	Schema = mongoose.Schema;

var InsuranceSchema = new Schema({
	created_at: {
		type: Date,
		default: Date.now
	},
	WFTDA: {
		type: String
		//required: true
	},
	other_accidental_medical_policy_name: {
		type: String
		//required: false,
        //validate: validator.general_string_long_validator
	},
	primary_carrier: {
		type: String
		//required: false,
        //validate: validator.general_string_long_validator
	},
	medical_conditions: {
		type: String
		//required: false
	},
	allergies: {
		type: String
		//required: false
	},

	/* TODO: Upload capability with URL
	rcr_waiver_liability_form: {

	} */
	WFTDA_confidentiality_acknowledgement_tracking: {
		type: Boolean
		//required: true
	},
	concussion_training_complete: {
		type: Boolean
		//required: true,
		//default: false
	}
});

mongoose.model('Insurance', InsuranceSchema);

exports.schema = InsuranceSchema;