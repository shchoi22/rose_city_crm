var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

//TODO: CREATED BY

var NoteSchema = new Schema({
	created_at: {
		type: Date,
		default: Date.now
	},
	created_by: {
		type: Schema.Types.ObjectId,
		ref: 'User'
	},
	attached_to: {
		type: Schema.Types.ObjectId,
		required: true
	},
	content: {
		type: String,
		required: false
	}
});

mongoose.model('Note', NoteSchema);