var mongoose = require('mongoose'),
    validator = require('../helpers/validator'),
    Schema = mongoose.Schema;

var TeamSchema = new Schema({
  name: {
    type: String,
    trim: true,
    unique: true,
    index: true,
    default: '',
    required: 'Please enter the name of the team.',
    validate: validator.general_string_short_validator
  },
  members: [{ type: Schema.Types.ObjectId, ref: 'Member' }]
});

mongoose.model('Team', TeamSchema);