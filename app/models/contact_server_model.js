var mongoose = require('mongoose'),
    validator = require('../helpers/validator'),
    Schema = mongoose.Schema;

var ContactSchema = new Schema({
	created_at: {
		type: Date,
		default: Date.now
	},
	contact_type: {
		type: String,
		enum: ['Guardian','Parent','Emergency','Alternative'],
		required: true
	},
	first_name: {
	    type: String
	    //required: true,
        //validate: validator.name_validator
	},
	last_name: {
	    type: String
	    //required: true,
        //validate: validator.name_validator
	},
	home_address: {
		type: String
		//required: false,
        //validate: validator.general_string_long_validator
	},
	home_address_2: {
		type: String
		//required: false,
        //validate: validator.general_string_long_validator
	},
	city: {
		type: String
		//required: false,
        //validate: validator.general_string_long_validator
	},
	state: {
		type: String, //TODO: Enum validation of states
		//required: false
	}, 
	zip_code: {
		type: String
		//required: false,
        //validate: validator.zipcode_validator
	},
	phone_number: {
		type: String
		//required: true,
        //validate: validator.phone_number_validator
	},
	email_address: {
		type: String
		//required: false,
        //validate: validator.email_address_validator
	}
});

mongoose.model('Contact', ContactSchema);

exports.schema = ContactSchema;