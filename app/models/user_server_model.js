var mongoose = require('mongoose'),
  Schema = mongoose.Schema,
  validator = require('../helpers/validator'),
  crypto = require('crypto');

var UserSchema = new Schema({
  email: {
    type: String,
    unique: true,
    required: true, 
    validate: validator.email_address_validator
  },
  username: {
    type: String,
    unique: true,
    required: true,
    validate: validator.general_string_short_validator
  },
  hashedPassword: {
    type: String,
    required: true
  },
  salt: String,
  role: {
    type: String,
    required: true,
    enum: ['user', 'coach', 'admin'],
    default: 'user'
  }
});

UserSchema
  .virtual('password')
  .set(function(password) {
    this._password = password;
    this.salt = this.makeSalt();
    this.hashedPassword = this.encryptPassword(password);
  })
  .get(function() {
    return this._password;
  });

UserSchema
  .virtual('user_info')
  .get(function () {
    return { '_id': this._id, 'username': this.username, 'email': this.email, 'role': this.role };
  });

UserSchema.methods = {
  /**
   * Authenticate - check if the passwords are the same
   */
  authenticate: function(plainText) {
    return this.encryptPassword(plainText) === this.hashedPassword;
  },

  makeSalt: function() {
    return crypto.randomBytes(16).toString('base64');
  },

  encryptPassword: function(password) {
    if(!password || !this.salt) return '';
    var salt = new Buffer(this.salt, 'base64');
    return crypto.pbkdf2Sync(password, salt, 10000, 64).toString('base64');
  }
};

mongoose.model('User', UserSchema);