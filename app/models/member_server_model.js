var mongoose = require("mongoose"),
    contacts = require('./contact_server_model'),
    insurances = require('./insurance_server_model'),
    validator = require('../helpers/validator'),
	Schema = mongoose.Schema;

var MemberSchema = new Schema({
	created_at: {
		type: Date,
		default: Date.now
	},
	first_name: {
	    type: String
	    //required: true,
        //validate: validator.name_validator
	},
	last_name: {
	    type: String
	    //required: true,
        //validate: validator.name_validator
	},
	is_active: {
	    type: Boolean
	    //default: true
	},
	member_type: {
		type: String,
		//required: true,
		enum: ['Skater','Alumni', 'Honorary','Referee','Non-Skating Official','Coach',
		      'Team Administrator','League Staff'],
		default: 'Skater'
	},
	affiliation: {
		type: Schema.Types.ObjectId, 
		ref: 'Team'
	},
	skate_name: {
		type: String
		//required: false,
		//default: null,
    //validate: validator.general_string_short_validator,
	},
	skate_number: {
		type: String,
		required: false,
		default: null
	},
	home_address: {
		type: String
		//required: true
	},
	home_address_2: {
		type: String,
		required: false
	},
	city: {
		type: String,
		required: false
    //validator: validator.general_string_short_validator
	},

	state: {
		type: String,
		//required: true,
		enum: [null, "AL", "AK", "AZ", "AR", "CA", "CO", "CT", "DC", "DE", "FL", "GA", "HI", "ID", "IL", "IN", "IA", "KS", "KY", "LA", "ME", "MD", "MA", "MI", "MN", "MS", "MO", "MT", "NE", "NV", "NH", "NJ", "NM", "NY", "NC", "ND", "OH", "OK", "OR", "PA", "RI", "SC", "SD", "TN", "TX", "UT", "VT", "VA", "WA", "WV", "WI", "WY"],
    default: 'OR'
	}, 
	zip_code: {
		type: String
		//required: true,
    //validate: validator.zipcode_validator
	},
	phone_number: {
		type: String
		//required: false,
    //validate: validator.phone_number_validator
	},
	email_address: {
		type: String
		//required: false,
    //validate: validator.email_address_validator,
	},
	date_of_birth: {
		type: Date
		//required: true,
    //validate: validator.date_of_birth_validator
	},
	gender: {
		type: String,
		enum: [null, 'Male', 'Female', 'Other'],
		//required: false,
		default: null
	},
	contacts: [contacts.schema],
	insurances: [insurances.schema],
	notes: [{
		type: Schema.Types.ObjectId,
		ref: 'Note'
	}],
	shirt_size: {
		type: String,
		enum: [null,'XS','S','M','L','XL','XXL'],
		required: false,
		default: null
	},
	skate_size: {
		type: String,
		//enum: [null,'a - 1','a - 2','a - 3','a - 4','a - 5','a - 6','a - 7','a - 8','a - 9',
		//       'b - 4','b - 5','b - 6','b - 7','b - 8','b - 9','b - 10','b - 11'],
		required: false,
		default: null
	},
	knee_pad_size: {
		type: String,
		enum: [null,'JR', 'XS','S', 'M', 'L' ,'XL' ,'XXL'],
		required: false,
		default: null
	},
	elbow_pad_size: {
		type: String,
		enum: [null,'JR', 'XS','S', 'M', 'L' ,'XL' ,'XXL'],
		required: false,
		default: null
	},
	wrist_size: {
		type: String,
		enum: [null,'JR', 'XS','S', 'M', 'L' ,'XL' ,'XXL'],
		required: false,
		default: null
	},
	helmet_size: {
		type: String,
		enum: [null,'KS','KM','KL','KXL','XS','S', 'M', 'L' ,'XL' ,'XXL'],
		required: false,
		default: null
	},
	occupation: {
		type: String
		//required: false,
		//default: null,
    //validate: validator.general_string_long_validator
	},
	employer: {
		type: String
		//required: false,
		//default: null,
    //validate: validator.general_string_long_validator
	}
});

mongoose.model('Member', MemberSchema);
