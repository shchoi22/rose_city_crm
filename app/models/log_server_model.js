var mongoose = require("mongoose"),
  validator = require('../helpers/validator'),
	Schema = mongoose.Schema;

var LogSchema = new Schema({
	created_at: {
		type: Date,
		default: Date.now
	},
	created_by: {
		type: Schema.Types.ObjectId,
		ref: 'User'
	},
	action:{
		type: String,
	},
	changed_id: {
		type: Schema.Types.ObjectId
	},
	changed_type: {
		type: String
	},
	diff: { 
		type : Object
	}
});

mongoose.model('Log', LogSchema);