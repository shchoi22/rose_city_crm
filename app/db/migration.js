process.env.NODE_ENV = process.env.NODE_ENV || 'development';

var basicCSV = require("basic-csv"),
    config = require('../../config/config'),
    mongoose = require('mongoose'),
    Schema = mongoose.Schema;

// models
var member = require('../models/member_server_model');
var user = require('../models/user_server_model');
var team = require('../models/team_server_model');
var note = require('../models/note_server_model');
var insurance = require('../models/insurance_server_model');

var Member = mongoose.model('Member'),
    Team = mongoose.model('Team'),
    Note = mongoose.model('Note'),
    Insurance = mongoose.model('Insurance'),
    Contact = mongoose.model('Contact');

mongoose.connect(config.db);

//Create teams
var team_list = ["101", "BNB", "FM", "GNR", "HH", "HR", "S&D", "WOJ", "RB", "RP"];
var team_ids = {};

for (var i = 0; i < team_list.length; i++) {
	var team_name = team_list[i];

	var new_team = new Team({name: team_name});

	team_ids[team_name] = new_team._id;

	new_team.save(function(err, data) {
		if(err) {
			console.log(err);
		}
	});
};


basicCSV.readCSV(__dirname+'/skaters_master_list.csv', {dropHeader: true}, function (error, rows) {

	for (var i = 0; i < rows.length; i++) {

		var member_data = {
		  	is_active: (rows[i][2].toLowerCase() == "y"),
		  	first_name: rows[i][7],
		  	last_name: rows[i][8],
		  	member_type: 'Skater',
		  	affiliation: ((rows[i][6] === '') ? null : team_ids[String(rows[i][6]).toUpperCase()]),
		  	skate_name: ((rows[i][9] === '') ? null : rows[i][9]),
		  	skate_number: ((rows[i][55] === '') ? null : rows[i][55]),
		  	home_address: rows[i][10],
		  	city: rows[i][11],
		  	state: ((rows[i][12] === '') ? 'OR' : String(rows[i][12]).toUpperCase().trim()),
		  	zip_code: rows[i][13],
		  	phone_number: rows[i][14],
		  	email_address: rows[i][15],
		  	date_of_birth: rows[i][16],
		  	//gender: ,
		  	//shirt_size: ,
		  	skate_size: ((rows[i][18] === '') ? null : rows[i][18]),
		  	knee_pad_size: ((rows[i][19] === '') ? null : rows[i][19]),
		  	elbow_pad_size: ((rows[i][20] === '') ? null : rows[i][20]),
		  	wrist_size: ((rows[i][21] ==='') ? null : rows[i][21]),
		  	helmet_size: ((rows[i][22] === '') ? null : rows[i][22]),
		  	occupation: rows[i][23],
		  	employer: rows[i][24],
		  	insurances: [],
		  	contacts: []
		  };

		  var emergency_contact_data = {
		  	contact_type: 'Emergency',
		  	first_name: rows[i][39],
		  	phone_number: rows[i][40]
		  };

		  var alternate_contact_data = {
		  	contact_type: 'Alternative',
		  	first_name: rows[i][41],
		  	phone_number: rows[i][42]
		  };

		  var parent_contact_data = {
		  	contact_type: 'Parent',
		  	first_name: rows[i][43],
		  	last_name: rows[i][44],
		  	home_address: rows[i][45],
		  	city: rows[i][46],
		  	state: ((rows[i][47] === '') ? 'OR' : String(rows[i][47]).toUpperCase().trim()),
		  	zip_code: rows[i][48],
		  	phone_number: rows[i][49],
		  	email_address: rows[i][50]
		  };

		  var insurance_data = {
		  	WFTDA: rows[i][30],
		  	primary_carrier: rows[i][31],
		  	other_accidental_medical_policy_name: rows[i][32],
		  	medical_conditions: rows[i][33],
		  	allergies: rows[i][34],
		  	WFTDA_confidentiality_acknowledgement_tracking: (rows[i][36].toLowerCase() == "yes" || rows[i][36].toLowerCase() == "signed"),
		  	concussion_training_complete: rows[i][37],
		  };

		  var note_data = [{
		  	content: rows[i][1]
		  	},{
		  		content: rows[i][62] 
		  }];

		  if (emergency_contact_data.first_name != '') {
		  	member_data.contacts.push(new Contact(emergency_contact_data));
		  }
		  if (alternate_contact_data.first_name != '') {
		  	member_data.contacts.push(new Contact(alternate_contact_data));
		  }
		  if(parent_contact_data.first_name != '') {
		  	member_data.contacts.push(new Contact(parent_contact_data));
		  }

		  if(insurance_data.WFTDA != '' ) {
		  	member_data.insurances.push(new Insurance(insurance_data));
		  }
		   
		  Member.create(member_data, function(err, member) {
		  	if (err) {console.log(err);}
		  
			Team.findById(member.affiliation, function(err, data){
		  	  if (!!data) {
		  	 	data.members.push(member._id);

		  		data.save(function(err) {
		  			if(err){
          			  console.log(err);
        		    }
		  		});
		  	  }
		    });
		});

		  /*
		  //var new_member = new Member(member_data);

		  

		  var member_id = new_member._id


		  new_member.save(function(err) {
		  	if (err) {
		  		console.log(err);
		  	}
		  });

		  */

	}
	console.log("done");
});

