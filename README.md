# Rose City Rollers CRM

This app helps Rose City Rollers manage their members (skaters, coaches, etc.).

## Tech Stack
 - Host: Heroku
 - Database: Mongodb 
 - Server: Node.js
 - Client: Angular.js
 - Style: Bootstrap

## Development
 1. Install Mongodb
 2. Clone repo and cd into app directory
 3. Install npm
 4. `$ npm install`
 5. `$ bower install`
 6. `$ node server`

## Test
 1. Follow development setup instructions above
 2. `$ NODE_ENV=test mocha --reporter spec app/tests`
 - Note: Currently there are only backend unit tests

## Production
 - [Here](http://rosecityrollers.herokuapp.com/)

