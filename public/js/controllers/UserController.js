angular.module('app')
  .controller('UsersController', ['$scope','$routeParams','UserService',
      function($scope, $routeParams, UserService) {
        UserService.query({}).$promise.then(function(data) {
            $scope.users = data;
        },function(errorResult) {
            console.log(errorResult);
        });
  }])
  .controller('UserController',['$scope','$routeParams','AuthService','UserService','$location', 
    function ($scope, $routeParams, AuthService, UserService, $location) {
      if($routeParams.id) {
        UserService.get({id: $routeParams.id}).$promise
          .then(function(data){
              $scope.user = data;
              console.log(data);
          },function(error){
              console.log(error);
          });
      };

      $scope.error = {};
      $scope.user = {};

      $scope.login = function(form) {
        AuthService.login('password', {
            'email': $scope.user.email,
            'password': $scope.user.password
          },
          function(err) {
            $scope.errors = {};

            if (!err) {
              $location.path('/');
            } else {
              angular.forEach(err.errors, function(error, field) {
                form[field].$setValidity('mongoose', false);
                $scope.errors[field] = error.type;
              });
              $scope.error.other = err.message;
            }
        });
      };
      
      $scope.register = function(form) {
        AuthService.createUser({
            email: $scope.user.email,
            username: $scope.user.username,
            password: $scope.user.password,
            role: $scope.user.role
          },
          function(err) {
            $scope.errors = {};
            if (!err) {
              $location.path('/users');
            } else {
              angular.forEach(err.errors, function(error, field) {
                form[field].$setValidity('mongoose', false);
                $scope.errors[field] = error.type;
              });
            }
          }
        );
      };
  }]);