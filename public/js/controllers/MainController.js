angular.module('app')
  .controller('MainController', ['$scope','$rootScope', 'AuthService', 'EnumService', 'MemberService',
   function($scope, $rootScope, AuthService, EnumService, MemberService) {
    if (!$scope.enums) {
      EnumService.get().$promise.then(function(data) {
        $scope.enums = data;
      });
    }

    $scope.download = function(resource){
      window.open(resource);
    };

    $scope.search_results = [];

    $scope.logout = function() {
      AuthService.logout(function(err) {
        if(!err) {
          $location.path('/login');
        }
      });
    };

    $scope.search = function() {
      if ($scope.search_text !== '') {
        MemberService.query({search_text: $scope.search_text}).$promise
        .then(function(data){
          $scope.search_results = data;
        },function(error){
          console.log(error);
        }); 
      }   
    };

    $scope.reset_search = function() {
      $scope.search_text = null;
      $scope.search_results = [];
    };
  }]);
