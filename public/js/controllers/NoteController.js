angular.module('app')
  .controller('NoteController', ['$scope','$rootScope', '$modal', '$modalInstance','NoteService', 'MemberService',
    function ($scope, $rootScope, $modal, $modalInstance, NoteService, MemberService) {
      $scope.ok = function () {
        $modalInstance.close($scope.member);
      };

      $scope.cancel = function () {
        $modalInstance.close($scope.member);
      };

      $scope.edit_cancel = function () {
        MemberService.get({id: $scope.member._id})
         .$promise.then(function(member) {
             $modalInstance.close(member);
          });
      };

      $scope.edit = function() {
        $scope.update="edit";
      };

      $scope.create = function () {
        $scope.note.attached_to = $scope.member._id;
        $scope.note.created_by = $scope.currentUser._id;
        NoteService.save({member_id: $scope.member._id}, $scope.note)
          .$promise.then(function(member) {
             $modalInstance.close(member);
           });
      };

      $scope.submit = function() {
       NoteService.update({
        member_id: $scope.member._id,
        note_id: $scope.selected_note._id}, $scope.selected_note)
        .$promise.then(function(note) {
          $modalInstance.close($scope.member);
        });
      };

      $scope.delete = function() {
        NoteService.delete({
          member_id: $scope.member._id,
          note_id: $scope.selected_note._id
        }).$promise.then(function(member) {
          $modalInstance.close(member);
        });
      };
  }]);