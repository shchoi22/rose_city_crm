angular.module('app')
  .controller('MembersController', ['$scope','$routeParams','MemberService',
      function($scope, $routeParams, MemberService){
        MemberService.query({}).$promise.then(function(data) {
            $scope.members = data;
        },function(errorResult) {
            console.log(errorResult);
          });
  }])

.controller('MemberController', ['$scope', '$modal','$routeParams','$location','MemberService', 'NoteService', 'LogService', 'flash',
    function($scope, $modal, $routeParams, $location, MemberService, NoteService, LogService, flash ){
      
        // Defaults
        $scope.member = {
          gender: 'Female',
          state: 'OR',
          member_type: 'Skater',
          city: 'Portland'
        };
          
        
        if($routeParams.id) {
            MemberService.get({id: $routeParams.id}).$promise
            .then(function(data){
                $scope.member = data;
            },function(error){
                console.log(error);
            });

            NoteService.get({member_id: $routeParams.id}).$promise
             .then(function(data) {
              $scope.notes = data;
             }, function(error) {
              console.log(error);
             });

            LogService.get({id: $routeParams.id}).$promise
             .then(function(data) {
              $scope.member_logs = data;
             }, function(errorResult) {
                console.log(errorResult)
            });
        }
      
        $scope.submit = function() {
          if ($routeParams.id) {
              MemberService.update({id:$scope.member._id}, $scope.member)
               .$promise.then(function(data) {
                   $location.path('/members/'+ data._id);
               }, function(error) {
              flash(error.data.message);
             });
          } else {
              MemberService.save($scope.member).$promise
               .then(function(data) {
                   $location.path('/members/'+ data._id);
               }, function(error) {
              flash(error.data.message);
             });
          }
        };
     
     $scope.clear = function(){
       $scope.member = {};
     };
      
      $scope.update = function() {
          MemberService.update({id: $scope.member._id}, $scope.member)//.$promise
          .$promise.then(function(data) {
              $location.path('/members/' + data._id);
          });
      };
      
      $scope.delete = function() {
          MemberService.delete({id:$scope.member._id}).$promise
          .then(function() {
              $location.path('/members');
          });
      };
      //CONTACT MODAL
      $scope.open = function (contact) {
        $scope.selected_contact = contact;
        var modalInstance = $modal.open({
          templateUrl: '../../views/contacts/show_modal.html',
          controller: 'ContactController',
          scope: $scope
        });

        modalInstance.result.then(function (member) {
            $scope.member = member;
            $scope.selected_contact = null;
        });
      };

      $scope.open_note = function(note) {
        $scope.selected_note = note;
        var modalInstance = $modal.open({
          templateUrl: '../../views/notes/show_note.html',
          controller: 'NoteController',
          scope: $scope
        });

        modalInstance.result.then(function(member) {
          $scope.member = member;
          NoteService.get({member_id: $scope.member._id}).$promise
             .then(function(data) {
              $scope.notes = data;
             }, function(error) {
                flash(error);
             }); 
          $scope.selected_note = null;
        });
      };

      $scope.open_insurance = function(insurance) {
        $scope.selected_insurance = insurance;
        var modalInstance = $modal.open({
          templateUrl: '../../views/insurances/show_insurance.html',
          controller: 'InsuranceController',
          scope: $scope
        });

        modalInstance.result.then(function (member) {
            $scope.member = member;
            $scope.selected_insurance = null;
        });
      };

      $scope.add_contact = function() {
        var modalInstance = $modal.open({
          templateUrl: '../../views/contacts/new_modal.html',
          controller: 'ContactController',
          scope: $scope
        });

        modalInstance.result.then(function (member) {
          $scope.member = member;
        });
      };

      $scope.add_note = function() {
        var modalInstance = $modal.open({
          templateUrl: '../../views/notes/new_note.html',
          controller: 'NoteController',
          scope: $scope
        });

        modalInstance.result.then(function (member) {
          $scope.member = member;
          NoteService.get({member_id: $scope.member._id}).$promise
             .then(function(data) {
              $scope.notes = data;
             }, function(error) {
              flash(error);
             }); 
        });
      };

      $scope.add_insurance = function() {
        var modalInstance = $modal.open({
          templateUrl: '../../views/insurances/new_insurance.html',
          controller: 'InsuranceController',
          scope: $scope
        });

        modalInstance.result.then(function (member) {
          $scope.member = member;
        });
      };

      $scope.open_date = function($event) {
        $event.preventDefault();
        $event.stopPropagation();

        $scope.opened = true;
      };

  }]);
