angular.module('app')
  .controller('TeamsController', ['$scope', '$modal', '$routeParams','TeamService',
      function($scope, $modal, $routeParams, TeamService){
        
        var getTeams = function(){
          TeamService.query().$promise.then(function(data) {
            $scope.teams = data;
          },function(errorResult) {
            console.log(errorResult);
          });
        };
        getTeams();
        
        $scope.addTeam = function() {
          $modal.open({
            templateUrl: '../../views/teams/new.html',
            controller: 'TeamModalController',
            resolve: {
              Team: function(){
                return TeamService;
              },
              id: function(){
                return null;
              }
            }
          }).result.then(function(){
            getTeams();
          });
        };
        $scope.deleteTeam = function( team ){
          $modal.open({
            templateUrl: '../../views/teams/delete.html',
            controller: 'TeamModalController',
            resolve: {
              Team: function(){
                return team;
              },
              id: function(){
                return team._id;
              }
            }
          }).result.then(function(result){          
            getTeams();

          });
        };
  }])

.controller('TeamController', ['$scope', '$modal','$routeParams','$location','TeamService',
    function($scope, $modal, $routeParams, $location, TeamService ){
      $scope.team = null;
      if($routeParams.id) {
        TeamService.get({id: $routeParams.id}).$promise
          .then(function(data){
            $scope.team = data;
          },function(error){
            console.log(error);
          });
      }

      $scope.addMembers = function() {
        var modalInstance = $modal.open({
          templateUrl: '../../views/teams/add_members.html',
          controller: 'AddMemberModalController',
          scope: $scope
        });

        modalInstance.result.then(function (team) {
            $scope.team = team;
        });
      }

      $scope.editTeam = function(){
        $modal.open({
          templateUrl: '../../views/teams/edit.html',
          controller: 'TeamModalController',
          resolve: {
            Team: function(){
              return TeamService;
            },
            id: function(){
              return $scope.team._id;
            }
          }
        }).result.then(function(team){
          $scope.team = team;
        });
      };
      
  }])
.controller('TeamModalController', ['$scope', '$modalInstance', 'Team', 'id',
  function($scope, $modalInstance,Team, id){
    $scope.team = {};
    $scope.create = function(){
      if(id){
        Team.update({id: id},{ 'name': $scope.team.name })
          .$promise.then(function(team) {
             $modalInstance.close(team);
           });
      }else{
        Team.save({ 'name': $scope.team.name })
          .$promise.then(function(team) {
             $modalInstance.close(team);
           });        
      }
    };
    $scope.delete = function(){
      Team.$remove()
        .then(function(team){
          $modalInstance.close(team);
      });
    };
    $scope.cancel = function () {
      $modalInstance.dismiss('cancel');
    };
  }])

.controller('AddMemberModalController', ['$scope','$modalInstance','MemberService', 'TeamService',
  function($scope, $modalInstance, MemberService, TeamService) {
    $scope.members = [];

    MemberService.query().$promise
     .then(function(data) {
      angular.forEach(data, function(member) {
        $scope.members.push(member);
      })
     });

    $scope.member_list = [];

    $scope.add_member_list = function() {
      var match = false;
      for(var i in $scope.members) {
        if($scope.members[i].first_name === $scope.member.first_name && $scope.members[i].last_name === $scope.member.last_name){
          $scope.member_list.push($scope.members[i]);
          match = true;
        }
      }
      if (!match) {
        alert('No match found');
      } else {
        $scope.member = null;
      }
    }

    $scope.remove_member_list = function(index) {
      $scope.member_list.splice(index, 1)
    }

    $scope.submit = function() {
      for(var i in $scope.member_list) {
        $scope.team.members.push($scope.member_list[i]);
      }

      TeamService.update({id: $scope.team._id}, $scope.team).$promise
          .then(function(data){
            $modalInstance.close(data);
          });
    }

    $scope.cancel = function() {
      $scope.member_list = [];

      $modalInstance.close($scope.team);
    }
}]);
