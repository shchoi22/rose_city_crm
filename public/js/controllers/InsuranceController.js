angular.module('app')
  .controller('InsuranceController', ['$scope','$rootScope','$modal','$modalInstance', 'InsuranceService', 'MemberService',
    function ($scope, $rootScope, $modal, $modalInstance, InsuranceService, MemberService) {
      $scope.ok = function () {
      	$modalInstance.close($scope.member);
      };

      $scope.cancel = function () {
      	$modalInstance.close($scope.member);
      };

      $scope.edit_cancel = function () {
        MemberService.get({id: $scope.member._id})
         .$promise.then(function(member) {
             $modalInstance.close(member);
          });
      };

      $scope.create = function () {
        InsuranceService.save({member_id: $scope.member._id}, $scope.insurance)
          .$promise.then(function(member) {
            $modalInstance.close(member);
          });
      };

      $scope.submit = function() {
       InsuranceService.update({
        member_id: $scope.member._id,
        insurance_id: $scope.selected_insurance._id}, $scope.selected_insurance)
        .$promise.then(function(member) {
          $modalInstance.close(member);
        });
      };

      $scope.edit = function() {
        $scope.update="edit";
      };

      $scope.delete = function() {
        InsuranceService.delete({
          member_id: $scope.member._id,
          insurance_id: $scope.selected_insurance._id
        }).$promise.then(function(member) {
          $modalInstance.close(member);
        });
      };
  }]);