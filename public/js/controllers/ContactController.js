angular.module('app')
  .controller('ContactController', ['$scope','$rootScope','$modal','$modalInstance', 'ContactService', 'MemberService',
    function ($scope, $rootScope, $modal, $modalInstance, ContactService, MemberService) {
      $scope.ok = function () {
      	$modalInstance.close($scope.member);
      };

      $scope.cancel = function () {
      	$modalInstance.close($scope.member);
      };

      $scope.edit_cancel = function () {
        MemberService.get({id: $scope.member._id})
         .$promise.then(function(member) {
             $modalInstance.close(member);
          });
      };

      $scope.create = function () {
        ContactService.save({member_id: $scope.member._id}, $scope.contact)
          .$promise.then(function(member) {
            $modalInstance.close(member);
          });
      };

      $scope.submit = function() {
       ContactService.update({
        member_id: $scope.member._id,
        contact_id: $scope.selected_contact._id}, $scope.selected_contact)
        .$promise.then(function(member) {
          $modalInstance.close(member);
        });
      };

      $scope.edit = function() {
        $scope.update="edit";
      };

      $scope.delete = function() {
        ContactService.delete({
          member_id: $scope.member._id,
          contact_id: $scope.selected_contact._id
        }).$promise.then(function(member) {
          $modalInstance.close(member);
        });
      };
  }]);