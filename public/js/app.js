angular.module('app', [
    'ngRoute', 
    'ngResource',
    'ngCookies',
    'ngSanitize',
    'http-auth-interceptor',
    'ui.bootstrap',
    'flash'
])
.config(['$routeProvider', '$locationProvider',
    function($routeProvider, $locationProvider){
      $routeProvider
        .when('/', {
          templateUrl: 'views/main/index.html',
          controller: 'MainController'
        })
        .when('/members', {
          templateUrl: 'views/members/index.html',
          controller: 'MembersController'
        })
      .when('/members/new', {
        templateUrl: 'views/members/form.html',
        controller: 'MemberController'
      })
      .when('/members/:id', {
        templateUrl: 'views/members/show.html',
        controller: 'MemberController'
      })
      .when('/teams', {
        templateUrl: 'views/teams/index.html',
        controller: 'TeamsController'
      })
      .when('/teams/:id', {
        templateUrl: 'views/teams/show.html',
        controller: 'TeamController'
      })
      .when('/login', {
        templateUrl: 'views/sessions/new.html',
        controller: 'UserController'
      })
      .when('/users', {
        templateUrl: 'views/users/index.html',
        controller: 'UsersController'
      })
      .when('/users/new', {
        templateUrl: 'views/users/new.html',
        controller: 'UserController'
      })
      .when('/users/:id', {
        templateUrl: 'views/users/show.html',
        controller: 'UserController'
      })
      .when('/reports', {
        templateUrl: 'views/reports/index.html',
        controller: 'MainController'
      })
      .otherwise({
        redirectTo: '/'
      });
    $locationProvider.html5Mode(true);
}])
.run(['$rootScope','$location','AuthService', function ($rootScope, $location, AuthService) {

    //watching the value of the currentUser variable.
     $rootScope.$on("$routeChangeStart", function(event, next, current) {
      if (!$rootScope.currentUser) {
        // no logged user, redirect to /login
        if (['views/sessions/new.html'].indexOf(next.templateUrl) == -1 ) {
            AuthService.currentUser();
        }
      }
     });
    
    $rootScope.$watch('currentUser', function(currentUser) {
      // if no currentUser and on a page that requires authorization then try to update it
      // will trigger 401s if user does not have a valid session
      if (!currentUser && (['/login'].indexOf($location.path()) == -1 )) {
        AuthService.currentUser();
      }
    });

    // On catching 401 errors, redirect to the login page.
    $rootScope.$on('event:auth-loginRequired', function() {
      $location.path('/login');
      return false;
    });
  }]);
  
