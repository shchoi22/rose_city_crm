angular.module('app')
  .factory('NoteService',['$resource', function($resource) {
  	return $resource('/api/members/:member_id/notes/:note_id',{
  		member_id: '@member_id',
  		note_id: '@note_id'
  	}, {
      'get': {
        isArray: true
      },
  		'update': {
  			method:'PUT'
  		}
  	});
  }])