angular.module('app')
  .factory('InsuranceService',['$resource', function($resource) {
  	return $resource('/api/members/:member_id/insurances/:insurance_id',{
  		member_id: '@member_id',
  		insurance_id: '@insurance_id'
  	}, {
  		'update': {
  			method:'PUT'
  		}
  	});
  }])