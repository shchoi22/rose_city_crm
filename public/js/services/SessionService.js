angular.module('app')
  .factory('SessionService',['$resource',function($resource){
      return $resource('/auth/session/');
  }]);
