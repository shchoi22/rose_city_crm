angular.module('app')
  .factory('ContactService',['$resource', function($resource) {
  	return $resource('/api/members/:member_id/contacts/:contact_id',{
  		member_id: '@member_id',
  		contact_id: '@contact_id'
  	}, {
  		'update': {
  			method:'PUT'
  		}
  	});
  }])