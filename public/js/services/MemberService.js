angular.module('app')
  .factory('MemberService',['$resource',function ($resource){
    return $resource('/api/members/:id', {id: '@_id'},{
        'update': { 
            method:'PUT' 
        },
        'query': {
            method: 'GET',
        	isArray: true
        }
    });
  }]);
