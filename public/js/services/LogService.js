angular.module('app')
  .factory('LogService',['$resource',function($resource){
    return $resource('/api/logs/members/:id', {id: '@_id'},{
        'get': { 
            isArray: true 
        }
    });
  }]);
