angular.module('app')
  .factory('TeamService',['$resource',function($resource){
    return $resource('/api/teams/:id', {id: '@_id'},{
        'update': { 
            method:'PUT' 
        }
    });
  }]);