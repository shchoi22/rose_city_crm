angular.module('app')
  .factory('EnumService',['$resource', function($resource) {
  	return $resource('/api/enums/',{});
  }])