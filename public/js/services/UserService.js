angular.module('app')
  .factory('UserService', ['$resource',function ($resource) {
    return $resource('/auth/users/:id', {id: '@_id'},
      {
        'update': {
          method:'PUT'
        },
        'query': {
        	method: 'GET',
        	isArray: true
        }
      });
  }]);