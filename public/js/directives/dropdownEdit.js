angular.module('app')
  .directive("dropdowneditable", ['MemberService', 'UserService', 'flash', function(MemberService, UserService, flash) {
    return {
      require: "ngModel",
      link: function(scope, element, attrs, ngModel) {
        element.bind("blur change", function() {
          if (scope.member) {
            if (scope.member.affiliation) {
              var affiliation_id = scope.member.affiliation._id;
              scope.member.affiliation = affiliation_id;
            }
            MemberService.update({id: scope.member._id}, scope.member)
              .$promise.then(function(data){
                flash('Updated successfully.');
              }, function(err){
                flash(err.data.message);
            });
          } else if (scope.user) {
            UserService.update({id: scope.user._id}, scope.user)
              .$promise.then(function(data){
                flash('Updated successfully.');
              }, function(err){
                flash(err.data.message);
            });
          }
        });
      }
    };
  }]);