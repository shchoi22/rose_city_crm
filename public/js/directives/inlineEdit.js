angular.module('app')
  .directive("contenteditable", ['MemberService', 'TeamService', 'flash', function(MemberService, TeamService, flash) {
    return {
      require: "ngModel",
      link: function(scope, element, attrs, ngModel) {

        function read() {
          ngModel.$setViewValue(element.html());
        }

        ngModel.$render = function() {
          element.html(ngModel.$viewValue || "");

        };

        element.bind("blur", function() {
          scope.$apply(read);
          if (scope.team) {
            TeamService.update({id: scope.team._id}, scope.team);
          } else if (scope.member) {
            MemberService.update({id: scope.member._id}, scope.member)
              .$promise.then(
                function(data){
                  flash('Change successful');
                },
                function(err){
                flash(err.data.message);
            });
          }
        });
      }
    };
  }]);